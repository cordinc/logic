package com.cordinc.logic

import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Assert.fail
import org.junit.Test

import junit.framework.TestCase;

class TermTest extends TestCase {
  
  @Test def testString: Unit = {
    assertEquals("f", Str("f").value)
    assertEquals("", Str("").value)
    assertFalse(Str("f").contains(Variable("F")))
    assertEquals(Set.empty[Variable], Str("f").extractVars)
    assertEquals(Str("f"), Str("f").substitute(Variable("F"), Str("g")))
  }
  
  @Test def testAtomArity: Unit = {
    assertEquals(0, Atom("f").arity)
  }
  
  @Test def testListArity: Unit = {
    assertEquals(2, ListTerm.empty.arity)
    assertEquals(2, Lst(List(Atom("a"))).arity)
    assertEquals(2, Lst(List(Atom("a"), Atom("b"))).arity)
    assertEquals(2, Lst(List(Atom("a"), Atom("b"), Atom("c"))).arity)
    assertEquals(2, HeadTailLst(Atom("a"), Atom("b")).arity)
    assertEquals(2, HeadTailLst(Atom("a"), HeadTailLst(Atom("b"), Atom("c"))).arity)
  }
  
  @Test def testVariableUnderscore: Unit = {
    assertEquals("_", Variable("_").name)
  }
  
  @Test def testPredicateArity: Unit = {
    assertEquals(1, Predicate("f", List(Variable("X"))).arity)
    assertEquals(2, Predicate("f", List(Variable("X"), Atom("b"))).arity)
    assertEquals(3, Predicate("f", List(Atom("a"), Variable("X"), Atom("b"))).arity)
    assertEquals(4, Predicate("f", List(Atom("a"), Atom("a"), Variable("X"), Atom("b"))).arity)
    assertEquals(2, Predicate("f", List(Variable("X"), Predicate("f", List(Atom("b"))))).arity)
  }
  
  @Test def testTrivialExtractVars: Unit = {
    assertEquals(Set.empty, Term.t.extractVars)
    assertEquals(Set.empty, Atom("d").extractVars)
    assertEquals(Set.empty, Number(5).extractVars)
  }
  
  @Test def testVarsExtractVars: Unit = {
    assertEquals(Set(Variable("X")), Variable("X").extractVars)
    assertEquals(Set(Variable("Y")), Variable("Y").extractVars)
  }
  
  @Test def testLstExtractVars: Unit = {
    assertEquals(Set.empty, Lst(List()).extractVars)
    assertEquals(Set.empty, Lst(List(Atom("a"), Atom("a"), Atom("a"))).extractVars)
    assertEquals(Set(Variable("X")), Lst(List(Atom("a"), Variable("X"))).extractVars)
    assertEquals(Set(Variable("Y")), Lst(List(Variable("Y"))).extractVars)
    assertEquals(Set(Variable("X"), Variable("Y"), Variable("Z")), Lst(List(Term.t, Variable("X"), Lst(List(Number(1.1d), Variable("Y"))), Variable("Z"))).extractVars)
    assertEquals(Set(Variable("X")), Lst(List(Variable("X"), Variable("X"))).extractVars)
  }
  
  @Test def testHeadTailLstExtractVars: Unit = {
    assertEquals(Set.empty, HeadTailLst(Atom("a"), Atom("b")).extractVars)
    assertEquals(Set(Variable("X")), HeadTailLst(Atom("a"), Variable("X")).extractVars)
    assertEquals(Set(Variable("Y")), HeadTailLst(Variable("Y"), Number(3)).extractVars)
    assertEquals(Set(Variable("X"), Variable("Y"), Variable("Z")), HeadTailLst(Term.t, HeadTailLst(Variable("X"), HeadTailLst(Lst(List(Number(1.1d), Variable("Y"))), Variable("Z")))).extractVars)
    assertEquals(Set(Variable("X")), HeadTailLst(Variable("X"), Variable("X")).extractVars)
  }
  
  @Test def testPredicateExtractVars: Unit = {
    assertEquals(Set.empty, Predicate("f", List(Atom("a"), Atom("a"), Atom("a"))).extractVars)
    assertEquals(Set(Variable("X")), Predicate("g", List(Atom("a"), Variable("X"))).extractVars)
    assertEquals(Set(Variable("X")), Predicate("h", List(Variable("X"), Variable("X"))).extractVars)
    assertEquals(Set(Variable("X"), Variable("Y"), Variable("Z")), Predicate("z", List(Term.t, Variable("X"), Predicate("f", List(Number(1.1d), Variable("Y"))), Variable("Z"))).extractVars)
  }
  
  @Test def testSingleContains: Unit = {
    assertFalse(Atom("y").contains(Variable("X")))  
    assertFalse(True().contains(Variable("X")))  
    assertFalse(False().contains(Variable("X")))  
    assertFalse(Number(1.1).contains(Variable("X")))  
  }
  
  @Test def testVariableContain: Unit = {
    assertTrue(Variable("X").contains(Variable("X")))  
    assertFalse(Variable("X").contains(Variable("Y")))  
  }
  
  @Test def testPredicateContain: Unit = {
   assertTrue(Predicate("f", List(Variable("X"))).contains(Variable("X")))  
   assertFalse(Predicate("f", List(Variable("Y"))).contains(Variable("X")))
   assertFalse(Predicate("f", List(Atom("x"))).contains(Variable("X")))
   assertTrue(Predicate("f", List(Atom("a"), Atom("b"), Variable("X"))).contains(Variable("X")))  
   assertTrue(Predicate("f", List(Atom("a"), Atom("b"), Predicate("g", List(Variable("X"))))).contains(Variable("X"))) 
   assertFalse(Predicate("f", List(Atom("a"), Atom("b"), Predicate("g", List(Variable("Y"))))).contains(Variable("X"))) 
   assertTrue(Predicate("f", List(Variable("X"), Variable("X"))).contains(Variable("X")))  
   assertTrue(Predicate("f", List(Atom("a"), Atom("b"), HeadTailLst(Atom("c"), Variable("X")))).contains(Variable("X"))) 
   assertTrue(Predicate("f", List(Atom("a"), Atom("b"), Lst(List(Atom("c"), Variable("X"))))).contains(Variable("X"))) 
  }
  
  @Test def testNullLstContain: Unit = {
    assertFalse(ListTerm.empty.contains(Variable("X"))) 
  }
  
  @Test def testLstContain: Unit = {
    assertTrue(Lst(List(Variable("X"))).contains(Variable("X")))  
    assertFalse(Lst(List(Variable("Y"))).contains(Variable("X"))) 
    assertTrue(Lst(List(Variable("Z"), Variable("Y"), Variable("X"))).contains(Variable("X"))) 
    assertFalse(Lst(List(Variable("Z"), Variable("Y"), Variable("X"))).contains(Variable("B"))) 
    assertTrue(Lst(List(Variable("Z"), Variable("Y"), Lst(List(Variable("A"), Variable("X"))))).contains(Variable("X"))) 
    assertTrue(Lst(List(Variable("Z"), Variable("Y"), Lst(List(Atom("a"), Lst(List(Variable("X"))))))).contains(Variable("X")))
    assertTrue(Lst(List(Atom("z"), Variable("Y"), Lst(List(Variable("X"), Lst(List(Variable("X"))))))).contains(Variable("X")))
    assertTrue(Lst(List(Atom("z"), Predicate("f", List(Variable("X"), Lst(List(Variable("X"))))))).contains(Variable("X")))
    assertTrue(Lst(List(Atom("z"), HeadTailLst(Variable("A"), Variable("X")))).contains(Variable("X")))
  }
  
  @Test def testHeadTailLstContain: Unit = {
    assertTrue(HeadTailLst(Variable("X"), Variable("Y")).contains(Variable("X")))  
    assertTrue(HeadTailLst(Variable("Y"), Variable("X")).contains(Variable("X")))  
    assertFalse(HeadTailLst(Variable("Y"), Variable("Y")).contains(Variable("X")))  
    assertTrue(HeadTailLst(Atom("a"), HeadTailLst(Atom("b"), Variable("X"))).contains(Variable("X")))  
    assertTrue(HeadTailLst(Variable("Z"), HeadTailLst(Variable("Y"), Variable("X"))).contains(Variable("X")))  
    assertTrue(HeadTailLst(Variable("Z"), Predicate("f", List(Variable("Y"), Variable("X")))).contains(Variable("X")))
    assertFalse(HeadTailLst(Variable("Z"), Predicate("f", List(Variable("Y"), Variable("XA")))).contains(Variable("X")))
    assertTrue(HeadTailLst(Variable("Z"), Predicate("f", List(Variable("Y"), Lst(List(Variable("X")))))).contains(Variable("X")))
  }
  
  @Test def testPredicateUnifiableWith: Unit = {
    assertFalse(Predicate("f", List(Variable("X"), Variable("Y"))).unifiableWith(Predicate("g", List(Variable("X"), Variable("Y")))))
    assertFalse(Predicate("f", List(Variable("X"), Variable("Y"))).unifiableWith(Predicate("f", List(Variable("X")))))
    assertTrue(Predicate("f", List(Variable("X"), Variable("Y"))).unifiableWith(Predicate("f", List(Variable("X"), Variable("X")))))
  }
  
  @Test def testSingleSubstitute: Unit = {
    assertEquals(Term.t, Term.t.substitute(Variable("X"), Term.f))   
    assertEquals(Term.f, Term.f.substitute(Variable("X"), Term.t))
    assertEquals(Number(1.1), Number(1.1).substitute(Variable("X"), Term.t))
    assertEquals(Atom("x"), Atom("x").substitute(Variable("X"), Term.t))
  }
  
  @Test def testVarSubstitute: Unit = {
    assertEquals(Term.t, Variable("X").substitute(Variable("X"), Term.t))
    assertEquals(Variable("Y"), Variable("X").substitute(Variable("X"), Variable("Y")))
    assertEquals(Variable("X"), Variable("X").substitute(Variable("Y"), Term.t))
  }
  
  @Test def testPredicateSubstitute: Unit = {
    assertEquals(Predicate("f", List(Atom("a"), Variable("Y"))), Predicate("f", List(Atom("a"), Variable("Y"))).substitute(Variable("X"), Term.t))
    assertEquals(Predicate("f", List(Atom("a"), Term.t)), Predicate("f", List(Atom("a"), Variable("Y"))).substitute(Variable("Y"), Term.t))
    assertEquals(Predicate("f", List(Atom("a"), Term.t, Term.t)), Predicate("f", List(Atom("a"), Variable("Y"), Variable("Y"))).substitute(Variable("Y"), Term.t))
    assertEquals(Predicate("f", List(Atom("a"), Term.t, Lst(List(Term.t)))), Predicate("f", List(Atom("a"), Variable("Y"), Lst(List(Variable("Y"))))).substitute(Variable("Y"), Term.t))
    assertEquals(Predicate("f", List(Atom("a"), Predicate("f", List(Atom("b"))))), Predicate("f", List(Atom("a"), Variable("Y"))).substitute(Variable("Y"), Predicate("f", List(Atom("b")))))
  }
  
  @Test def testNullLstSubstitute: Unit = {
    assertEquals(ListTerm.empty, ListTerm.empty.substitute(Variable("X"), Term.t))
  }
  
  @Test def testLstSubstitute: Unit = {
    assertEquals(Lst(List(Atom("x"))), Lst(List(Atom("x"))).substitute(Variable("X"), Term.t))
    assertEquals(Lst(List(Term.t)), Lst(List(Variable("X"))).substitute(Variable("X"), Term.t))
    assertEquals(Lst(List(Term.t, Atom("y"), Variable("Y"), Term.t)), Lst(List(Variable("X"), Atom("y"), Variable("Y"), Variable("X"))).substitute(Variable("X"), Term.t))
    assertEquals(Lst(List(Term.t, Atom("y"), Lst(List(Variable("Y"), Term.t)))), Lst(List(Variable("X"), Atom("y"), Lst(List(Variable("Y"), Variable("X"))))).substitute(Variable("X"), Term.t))
  }
  
  @Test def testHeadTailSubstitute: Unit = {
    assertEquals(HeadTailLst(Atom("x"), Atom("y")), HeadTailLst(Atom("x"), Atom("y")).substitute(Variable("X"), Term.t))
    assertEquals(HeadTailLst(Term.t, Atom("y")), HeadTailLst(Variable("X"), Atom("y")).substitute(Variable("X"), Term.t))
    assertEquals(HeadTailLst(Term.t, Term.t), HeadTailLst(Variable("X"), Variable("X")).substitute(Variable("X"), Term.t))  
    assertEquals(HeadTailLst(Atom("y"), HeadTailLst(Term.t, Variable("Y"))), HeadTailLst(Atom("y"), HeadTailLst(Variable("X"), Variable("Y"))).substitute(Variable("X"), Term.t))
  }
  
  @Test def testUnify: Unit = {
    assertEquals(Some(Substitutions(Map())), Atom("a").unifyWith(Atom("a")))
    assertEquals(None, Atom("a").unifyWith(Atom("b")))
    assertEquals(Some(Substitutions(Map())), Variable("X").unifyWith(Variable("X")))
    assertEquals(Some(Substitutions(Map(Variable("X") -> Variable("Y")))), Variable("X").unifyWith(Variable("Y")))
    assertEquals(Some(Substitutions(Map())), Predicate("dd", List(Variable("X"))).unifyWith(Predicate("dd", List(Variable("X")))))
    assertEquals(Some(Substitutions(Map(Variable("X") -> Variable("Y")))), Predicate("dd", List(Variable("X"))).unifyWith(Predicate("dd", List(Variable("Y")))))
    assertEquals(None, Predicate("dd", List(Variable("X"), Variable("Zs"))).unifyWith(Predicate("dd", List(Variable("Y")))))
  }
  
  @Test def testNotContains: Unit = {
    assertTrue(Not(Variable("X")).contains(Variable("X"))) 
    assertFalse(Not(Variable("Y")).contains(Variable("X")))  
  }
  
  @Test def testNotExtract: Unit = {
    assertEquals(Set.empty, Not(Atom("a")).extractVars)
    assertEquals(Set(Variable("X")), Not(Variable("X")).extractVars)
  }
  
  @Test def testNotUnify: Unit = {
    assertEquals(Some(Substitutions(Map())), Not(Atom("a")).unifyWith(Not(Atom("a"))))
    assertEquals(None, Not(Atom("a")).unifyWith(Not(Atom("b"))))
    assertEquals(Some(Substitutions(Map())), Not(Variable("X")).unifyWith(Not(Variable("X"))))
    assertEquals(Some(Substitutions(Map(Variable("X") -> Variable("Y")))), Not(Variable("X")).unifyWith(Not(Variable("Y"))))
  }
  
  @Test def testNotSubstitute: Unit = {
    assertEquals(Not(Atom("x")), Not(Atom("x")).substitute(Variable("X"), Atom("z")))
    assertEquals(Not(Atom("z")), Not(Variable("X")).substitute(Variable("X"), Atom("z")))
  }
  
  @Test def testOperatorArity: Unit = {
    assertEquals(Operation(Gt(), List(Number(5), Number(6))).arity, 2)
  }
  
  @Test def testOperatorContains: Unit = {
    assertFalse(Operation(Gt(), List(Number(5), Number(6))).contains(Variable("X"))) 
    assertTrue(Operation(Gt(), List(Number(5), Variable("Y"))).contains(Variable("Y")))  
  }
  
  @Test def testOperatorExtract: Unit = {
    assertEquals(Set.empty, Operation(Gt(), List(Number(5), Number(6))).extractVars)
    assertEquals(Set(Variable("Y")), Operation(Gt(), List(Number(5), Variable("Y"))).extractVars)
  }
  
  @Test def testOperatorSubstitute: Unit = {
    assertEquals(Operation(Gt(), List(Number(5), Variable("Y"))).substitute(Variable("X"), Number(7)), Operation(Gt(), List(Number(5), Variable("Y"))))
    assertEquals(Operation(Gt(), List(Number(5), Number(6))).substitute(Variable("X"), Number(7)), False())
    assertEquals(Operation(Gt(), List(Number(5), Variable("X"))).substitute(Variable("X"), Number(3)), True())
    assertEquals(Operation(Gt(), List(Variable("Y"), Variable("X"))).substitute(Variable("X"), Number(7)), Operation(Gt(), List(Variable("Y"), Number(7))))
  }
  
  @Test def testOperatorUnify: Unit = {
    assertEquals(None, Operation(Gt(), List(Variable("Y"), Variable("X"))).unifyWith(Not(Atom("a"))))
    assertEquals(Some(Substitutions(Map())), Operation(Gt(), List(Number(5), Number(6))).unifyWith(Operation(Gt(), List(Number(5), Number(6)))))
    assertEquals(Some(Substitutions(Map(Variable("X") -> Operation(Gt(), List(Number(5), Number(6)))))), Operation(Gt(), List(Number(5), Number(6))).unifyWith(Variable("X")))
    assertEquals(Some(Substitutions(Map(Variable("X") -> Number(6), Variable("Y") -> Number(5)))), Operation(Gt(), List(Number(5), Number(6))).unifyWith(Operation(Gt(), List(Variable("Y"), Variable("X")))))
  }
  
  @Test def testPrint: Unit = {
    assertEquals("dd(X,a,!,[not 5.0|[\"ff\",>(Y,X),true,false]])", Predicate("dd", List(Variable("X"), Atom("a"), Cut(None), HeadTailLst(Not(Number(5)),Lst(List(Str("ff"),Operation(Gt(), List(Variable("Y"), Variable("X"))), True(), False()))))).toString)
  }
  
  @Test def testEmptyOperatorNotAllowed: Unit = {
    try {
      Operation(Gt(), List())
      fail
    } catch {
      case e: IllegalArgumentException => {}
      case _ : Throwable => fail
    }
  }
  
  @Test def testEmptyPredicateNotAllowed: Unit = {
    try {
      Predicate("f", List.empty)
      fail
    } catch {
      case e: IllegalArgumentException => {}
      case _ : Throwable => fail
    }
  }
  
  @Test def testUnamedPredicateNotAllowed: Unit = {
    try {
      Predicate("", List(Atom("x")))
      fail
    } catch {
      case e: IllegalArgumentException => {}
      case _ : Throwable => fail
    }
  }
  
  @Test def testUnamedAtomNotAllowed: Unit = {
    try {
      Atom("")
      fail
    } catch {
      case e: IllegalArgumentException => {}
      case _ : Throwable => fail
    }
  }
  
  @Test def testCapitalisedAtomNotAllowed: Unit = {
    try {
      Atom("X")
      fail
    } catch {
      case e: IllegalArgumentException => {}
      case _ : Throwable => fail
    }
  }
  
  @Test def testUncapitalisedVariableNotAllowed: Unit = {
    try {
      Variable("x")
      fail
    } catch {
      case e: IllegalArgumentException => {}
      case _ : Throwable => fail
    }
  }
  
  @Test def testCapitalisedPredicateNotAllowed: Unit = {
    try {
      Predicate("X", List(Atom("x")))
      fail
    } catch {
      case e: IllegalArgumentException => {}
      case _ : Throwable => fail
    }
  }

  @Test def testCompactBasic: Unit = {
    assertEquals(Atom("a"), Atom("a").compact)
    assertEquals(Variable("A"), Variable("A").compact)
    assertEquals(Number(2), Number(2).compact)
    assertEquals(Str("what"), Str("what").compact)
    assertEquals(Cut(None), Cut(None).compact)
    assertEquals(True(), True().compact)
    assertEquals(False(), False().compact)
  }
  
  @Test def testCompactNot: Unit = {
    assertEquals(Not(Atom("a")), Not(Atom("a")).compact)
    assertEquals(Atom("a"), Not(Not(Atom("a"))).compact)
  }
  
  @Test def testCompactOp: Unit = {
    assertEquals(Operation(Gt(), List(Atom("a"), Atom("b"))), Operation(Gt(), List(Atom("a"), Atom("b"))).compact)
    assertEquals(Operation(Add(), List(Atom("a"), Atom("b"))), Operation(Add(), List(Atom("a"), Not(Not(Atom("b"))))).compact)
  }
  
  @Test def testCompactPredicate: Unit = {
    assertEquals(Predicate("a", List(Atom("a"), Atom("b"))), Predicate("a", List(Atom("a"), Atom("b"))).compact)
    assertEquals(Predicate("b", List(Atom("a"), Atom("b"))), Predicate("b", List(Atom("a"), Not(Not(Atom("b"))))).compact)
  }
  
  @Test def testCompactLst: Unit = {
    assertEquals(Lst(List(Atom("a"), Atom("b"))), Lst(List(Atom("a"), Atom("b"))).compact)
    assertEquals(Lst(List(Atom("a"), Atom("b"))), Lst(List(Atom("a"), Not(Not(Atom("b"))))).compact)
  }
  
  @Test def testCompactHeadTailLst: Unit = {
    assertEquals(HeadTailLst(Atom("a"), Variable("A")), HeadTailLst(Atom("a"), Variable("A")).compact)
    assertEquals(HeadTailLst(Variable("A"), Atom("a")), HeadTailLst(Variable("A"), Atom("a")).compact)
    assertEquals(HeadTailLst(Variable("A"), Variable("B")), HeadTailLst(Variable("A"), Variable("B")).compact)
    
    assertEquals(Lst(List(Atom("a"), Atom("b"))), HeadTailLst(Atom("a"), Atom("b")).compact)
    assertEquals(Lst(List(Atom("a"), Predicate("a", List(Atom("a"), Lst(List(Atom("a"), Atom("b"))))))) , HeadTailLst(Atom("a"), Predicate("a", List(Atom("a"), HeadTailLst(Atom("a"), Atom("b"))))).compact)
  
    assertEquals(Lst(List(Atom("a"), Atom("b"), Atom("c"), Atom("d"))), HeadTailLst(Atom("a"), HeadTailLst(Atom("b"), HeadTailLst(Atom("c"), Atom("d")))).compact)

    assertEquals(Lst(List(Atom("b"), Atom("x"), Lst(List(Atom("c"), Atom("a"))))), HeadTailLst(Atom("b"), Lst(List(Atom("x"), HeadTailLst(Atom("c"), Atom("a"))))).compact)
    
    assertEquals(HeadTailLst(Atom("a"), HeadTailLst(Variable("X"), Atom("d"))), HeadTailLst(Atom("a"), HeadTailLst(Variable("X"), Atom("d"))).compact)
  }
}