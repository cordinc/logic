package com.cordinc.logic

import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test

import junit.framework.TestCase;

class FamilyProgramTest extends TestCase {
  implicit val tracer = new NullTracer()
  
  val family = Program(List(
    HornClause(Predicate("auntOrUncle", List(Variable("X"), Variable("Y"))), List(Not(Predicate("parent", List(Variable("X"), Variable("Y")))), Predicate("parent", List(Variable("G"), Variable("X"))), Predicate("parent", List(Variable("G"), Variable("P"))), Predicate("parent", List(Variable("P"), Variable("Y"))))),
   
    HornClause(Predicate("parent", List(Variable("X"), Variable("Y"))), List(Predicate("father", List(Variable("X"), Variable("Y"))))),   
    HornClause(Predicate("parent", List(Variable("X"), Variable("Y"))), List(Predicate("mother", List(Variable("X"), Variable("Y"))))),
    HornClause(Predicate("grandparent", List(Variable("X"), Variable("Y"))), List(Predicate("mother", List(Variable("X"), Variable("Z"))), Predicate("mother", List(Variable("Z"), Variable("Y"))))),
    HornClause(Predicate("grandparent", List(Variable("X"), Variable("Y"))), List(Predicate("mother", List(Variable("X"), Variable("Z"))), Predicate("father", List(Variable("Z"), Variable("Y"))))),
    HornClause(Predicate("grandparent", List(Variable("X"), Variable("Y"))), List(Predicate("father", List(Variable("X"), Variable("Z"))), Predicate("mother", List(Variable("Z"), Variable("Y"))))),
    HornClause(Predicate("grandparent", List(Variable("X"), Variable("Y"))), List(Predicate("father", List(Variable("X"), Variable("Z"))), Predicate("father", List(Variable("Z"), Variable("Y"))))),
    HornClause(Predicate("child", List(Variable("X"), Variable("Y"))), List(Predicate("parent", List(Variable("Y"), Variable("X"))))),   
    
    HornClause(Predicate("auntOrUncle1", List(Variable("X"), Variable("Y"))), List(Not(Predicate("parent", List(Variable("X"), Variable("Y")))), Predicate("parent", List(Variable("G"), Variable("X"))), Predicate("parent", List(Variable("G"), Variable("P"))), Predicate("parent", List(Variable("P"), Variable("Y"))))),
    HornClause(Predicate("auntOrUncle2", List(Variable("X"), Variable("Y"))), List(Predicate("parent", List(Variable("G"), Variable("X"))), Predicate("parent", List(Variable("G"), Variable("P"))), Predicate("parent", List(Variable("P"), Variable("Y"))), Not(Predicate("parent", List(Variable("X"), Variable("Y")))))),
   
    HornClause.fact(Predicate("father", List(Atom("frank"), Atom("robert")))), 
    HornClause.fact(Predicate("father", List(Atom("roger"), Atom("frank")))),
    HornClause.fact(Predicate("father", List(Atom("roger"), Atom("jeremy")))),
    HornClause.fact(Predicate("mother", List(Atom("mary"), Atom("frank")))),
    HornClause.fact(Predicate("mother", List(Atom("sue"), Atom("robert")))),
    HornClause.fact(Predicate("father", List(Atom("tim"), Atom("sue")))),
    HornClause.fact(Predicate("mother", List(Atom("kate"), Atom("sue"))))
  ))
  
  @Test def testUncle: Unit =  { 
    val result = family.solve(Predicate("auntOrUncle", List(Atom("jeremy"), Atom("robert"))))
    assertTrue(result.hasSolution) 
    assertEquals(Substitutions.trivial, result.solution)  
    assertTrue(result.maybeMore)  
    
    val next = result.nextResult
    assertFalse(next.hasSolution) 
    assertFalse(next.maybeMore)
    assertEquals(None, next.solution) 
  }
  
  @Test def testUncle1: Unit =  { 
    val result = family.solve(Predicate("auntOrUncle1", List(Atom("jeremy"), Atom("robert"))))
    assertTrue(result.hasSolution) 
    assertEquals(Substitutions.trivial, result.solution)  
    assertTrue(result.maybeMore)  
    val next = result.nextResult
    assertFalse(next.hasSolution) 
    assertFalse(next.maybeMore)
    assertEquals(None, next.solution) 
  }
  
  @Test def testUncle2: Unit =  { 
    val result = family.solve(Predicate("auntOrUncle2", List(Atom("jeremy"), Atom("robert"))))
    assertTrue(result.hasSolution) 
    assertEquals(Substitutions.trivial, result.solution)  
    assertTrue(result.maybeMore)  
    val next = result.nextResult
    assertFalse(next.hasSolution) 
    assertFalse(next.maybeMore)
    assertEquals(None, next.solution) 
  }
  
  @Test def testUncleWhenParent: Unit =  { 
    val result = family.solve(Predicate("auntOrUncle", List(Atom("frank"), Atom("robert"))))
    assertFalse(result.hasSolution)  
  }
  
  @Test def testVarUncle: Unit =  { 
    assertEquals(None, family.solve(Predicate("auntOrUncle", List(Variable("X"), Atom("robert")))).solution)
  }
  
  @Test def testVarUncle2: Unit =  { 
    val result = family.solve(Predicate("auntOrUncle2", List(Variable("X"), Atom("robert"))))
    assertTrue(result.hasSolution) 
    assertEquals(Some(Substitutions(Map(Variable("X") -> Atom("jeremy")))), result.solution)  
    assertTrue(result.maybeMore) 
    val next = result.nextResult
    assertFalse(next.hasSolution) 
    assertFalse(next.maybeMore)
    assertEquals(None, next.solution) 
  }
  
  @Test def testNonParent: Unit =  {
    assertEquals(None, family.solve(Predicate("parent", List(Atom("frank"), Atom("frank")))).solution)       
    assertEquals(None, family.solve(Predicate("parent", List(Atom("frank"), Atom("sue")))).solution)       
    assertEquals(None, family.solve(Predicate("parent", List(Atom("robert"), Atom("frank")))).solution)       
    assertEquals(None, family.solve(Predicate("parent", List(Atom("frank"), Atom("geoffrey")))).solution)
  }
  
  @Test def testParent: Unit =  {
    assertEquals(Substitutions.trivial, family.solve(Predicate("parent", List(Atom("frank"), Atom("robert")))).solution)       
    assertEquals(Substitutions.trivial, family.solve(Predicate("parent", List(Atom("sue"), Atom("robert")))).solution)
  }
  
  @Test def testChild: Unit =  {
    assertEquals(Substitutions.trivial, family.solve(Predicate("child", List(Atom("robert"), Atom("frank")))).solution)       
    assertEquals(Substitutions.trivial, family.solve(Predicate("child", List(Atom("robert"), Atom("sue")))).solution)
  }
  
  @Test def testVarChild: Unit =  {
    assertFalse(family.solve(Predicate("child", List(Variable("X"), Atom("robert")))).hasSolution)  
    assertEquals(Some(Substitutions(Map(Variable("X") -> Atom("robert")))), family.solve(Predicate("child", List(Variable("X"), Atom("sue")))).solution)  
  }
  
  @Test def testVarParent: Unit =  { 
    val result = family.solve(Predicate("parent", List(Variable("X"), Atom("robert"))))
    assertTrue(result.hasSolution) 
    assertEquals(Some(Substitutions(Map(Variable("X") -> Atom("frank")))), result.solution)  
    assertTrue(result.maybeMore)  
    assertEquals(Some(Substitutions(Map(Variable("X") -> Atom("sue")))), result.nextResult.solution) 
  }
  
  @Test def testChaining: Unit =  {
    val result1 = family.solve(Predicate("parent", List(Variable("X"), Atom("robert"))))
    assertTrue(result1.hasSolution) 
    assertTrue(result1.maybeMore)
    assertEquals(Some(Substitutions(Map(Variable("X") -> Atom("frank")))), result1.solution) 
    
    val result2 = result1.nextResult
    assertTrue(result2.hasSolution) 
    assertTrue(result2.maybeMore)
    assertEquals(Some(Substitutions(Map(Variable("X") -> Atom("sue")))), result2.solution) 
    
    val result3 = result2.nextResult
    assertFalse(result3.hasSolution) 
    assertFalse(result3.maybeMore)
    assertEquals(None, result3.solution) 
    
    val result4 = result3.nextResult
    assertFalse(result4.hasSolution) 
    assertFalse(result4.maybeMore)
    assertEquals(None, result4.solution) 
  }
  
  @Test def testFindAllNone: Unit =  { 
    val result = family.findAll(Predicate("grandparent", List(Variable("X"), Atom("john"))))
    assertEquals(1, result.size)
    assertFalse(result(0).hasSolution)
    assertFalse(result(0).maybeMore)
  }
  
  @Test def testFindAllOne: Unit =  { 
    val result = family.findAll(Predicate("grandparent", List(Atom("roger"), Variable("X"))))
    assertEquals(2, result.size)
    assertTrue(result(0).hasSolution)
    assertTrue(result(0).maybeMore)
    assertEquals(Some(Substitutions(Map(Variable("X") -> Atom("robert")))), result(0).solution)
    
    assertFalse(result(1).hasSolution)
    assertFalse(result(1).maybeMore)
    assertEquals(None, result(1).solution)
  }
  
  @Test def testFindAllMany: Unit =  { 
    val result = family.findAll(Predicate("grandparent", List(Variable("X"), Atom("robert"))))
    assertEquals(5, result.size)
    
    val sols = result.map(_.solution)
    assertTrue(sols.contains(Some(Substitutions(Map(Variable("X") -> Atom("roger"))))))
    assertTrue(sols.contains(Some(Substitutions(Map(Variable("X") -> Atom("mary"))))))
    assertTrue(sols.contains(Some(Substitutions(Map(Variable("X") -> Atom("tim"))))))
    assertTrue(sols.contains(Some(Substitutions(Map(Variable("X") -> Atom("kate"))))))
    
    assertTrue(result(0).hasSolution)
    assertTrue(result(0).maybeMore)
    assertTrue(result(1).hasSolution)
    assertTrue(result(1).maybeMore)
    assertTrue(result(2).hasSolution)
    assertTrue(result(2).maybeMore)
    assertTrue(result(3).hasSolution)
    assertTrue(result(3).maybeMore)
    assertFalse(result(4).hasSolution)
    assertFalse(result(4).maybeMore)
  }
  
}
