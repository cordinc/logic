package com.cordinc.logic

import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Assert.fail
import org.junit.Test

import junit.framework.TestCase;

class OperatorTest extends TestCase {
  implicit val tracer = new NullTracer()
  
  val program = Program(List(
      HornClause(Predicate("a", List(Variable("X"))), List(Predicate("b", List(Variable("X"))), Operation(Gt(), List(Variable("X"), Number(6))))),
      HornClause.fact(Predicate("b", List(Number(7))))
  ))
  
  @Test def testNonExistant: Unit =  {
    val result = program.solve(Predicate("auntOrUncle", List(Atom("jeremy"), Atom("robert"))))
    assertEquals(None, result.solution)  
  }
  
  @Test def testGuardSucceed: Unit =  {
    val result = program.solve(Predicate("a", List(Number(7))))
    assertEquals(Substitutions.trivial, result.solution)  
  }
  
  @Test def testGuardFail: Unit =  {
    val result = program.solve(Predicate("a", List(Number(5))))
    assertEquals(None, result.solution)  
  }

  @Test def testVarGuard: Unit =  {
    val result = Program(List(
      HornClause(Predicate("a", List(Variable("X"))), List(Predicate("b", List(Variable("X"))), Operation(Gt(), List(Variable("X"), Number(4))))),
      HornClause.fact(Predicate("b", List(Number(4)))),
      HornClause.fact(Predicate("b", List(Number(5))))
    )).solve(Predicate("a", List(Variable("A"))))
    assertEquals(Some(Substitutions(Map(Variable("A") -> Number(5)))), result.solution)
    assertTrue(result.maybeMore)
    assertFalse(result.nextResult.hasSolution)
  }
  
  @Test def testTooManyParamsFailure: Unit =  {
    try {
      Operation(Gt(), List(Variable("Y"), Variable("X"), Variable("X")))
      fail
    } catch {
      case e: IllegalArgumentException => {}
      case _ : Throwable => fail
    }
  }
  
  @Test def testTooFewParamsFailure: Unit =  {
    try {
      Operation(Add(), List(Variable("Y")))
      fail
    } catch {
      case e: IllegalArgumentException => {}
      case _ : Throwable => fail
    }
  }
  
  @Test def testBlankParamsFailure: Unit =  {
    try {
      Operation(Eq(), List())
      fail
    } catch {
      case e: IllegalArgumentException => {}
      case _ : Throwable => fail
    }
  }
  
  @Test def testPrint: Unit =  {
    assertEquals(">(Y,X)", Operation(Gt(), List(Variable("Y"), Variable("X"))).toString)
    assertEquals("+(Y,X)", Operation(Add(), List(Variable("Y"), Variable("X"))).toString)
    assertEquals("=(Y,X)", Operation(Eq(), List(Variable("Y"), Variable("X"))).toString)
  }
  
  @Test def testGt: Unit =  {
    val p = Program(List(
      HornClause(Predicate("a", List(Variable("X"))), List(Operation(Gt(), List(Variable("X"), Number(6)))))
    ))
    val r1 = p.solve(Predicate("a", List(Number(7))))
    assertEquals(Substitutions.trivial, r1.solution)  
    val r2 = p.solve(Predicate("a", List(Number(6))))
    assertEquals(None, r2.solution)  
  }
  
  @Test def testGtEq: Unit =  {
    val p = Program(List(
      HornClause(Predicate("a", List(Variable("X"))), List(Operation(GtEq(), List(Variable("X"), Number(6)))))
    ))
    val r1 = p.solve(Predicate("a", List(Number(7))))
    assertEquals(Substitutions.trivial, r1.solution)  
    val r2 = p.solve(Predicate("a", List(Number(6))))
    assertEquals(Substitutions.trivial, r2.solution)  
    val r3 = p.solve(Predicate("a", List(Number(5))))
    assertEquals(None, r3.solution) 
  }
  
  @Test def testLt: Unit =  {
    val p = Program(List(
      HornClause(Predicate("a", List(Variable("X"))), List(Operation(Lt(), List(Variable("X"), Number(6)))))
    ))
    val r1 = p.solve(Predicate("a", List(Number(7))))
    assertEquals(None, r1.solution)  
    val r2 = p.solve(Predicate("a", List(Number(5))))
    assertEquals(Substitutions.trivial, r2.solution)  
  }
  
  @Test def testLtEq: Unit =  {
    val p = Program(List(
      HornClause(Predicate("a", List(Variable("X"))), List(Operation(LtEq(), List(Variable("X"), Number(6)))))
    ))
    val r1 = p.solve(Predicate("a", List(Number(7))))
    assertEquals(None, r1.solution)  
    val r2 = p.solve(Predicate("a", List(Number(6))))
    assertEquals(Substitutions.trivial, r2.solution)  
    val r3 = p.solve(Predicate("a", List(Number(5))))
    assertEquals(Substitutions.trivial, r3.solution) 
  }
  
  @Test def testEqNumber: Unit =  {
    val p = Program(List(
      HornClause(Predicate("a", List(Variable("X"))), List(Operation(Eq(), List(Variable("X"), Number(6)))))
    ))
    val r1 = p.solve(Predicate("a", List(Number(7))))
    assertEquals(None, r1.solution)  
    val r2 = p.solve(Predicate("a", List(Number(6))))
    assertEquals(Substitutions.trivial, r2.solution)  
  }
  
  @Test def testEqString: Unit =  {
    val p = Program(List(
      HornClause(Predicate("a", List(Variable("X"))), List(Operation(Eq(), List(Variable("X"), Str("abc")))))
    ))
    val r1 = p.solve(Predicate("a", List(Str("abcd"))))
    assertEquals(None, r1.solution)  
    val r2 = p.solve(Predicate("a", List(Str("abc"))))
    assertEquals(Substitutions.trivial, r2.solution)  
  }
  
  @Test def testDivideAdd: Unit =  {
    val p = Program(List(  
      HornClause(Predicate("a", List(Variable("X"))), List(Operation(Eq(), List(Number(6), Operation(Add(), List(Operation(Div(), List(Variable("X"), Number(2))), Number(1))))))) 
    ))
    val r1 = p.solve(Predicate("a", List(Number(7))))
    assertEquals(None, r1.solution) 
    val r2 = p.solve(Predicate("a", List(Number(10))))
    assertEquals(Substitutions.trivial, r2.solution)  
  }
  
  @Test def testMultSubtract: Unit =  {
    val p = Program(List(  
      HornClause(Predicate("a", List(Variable("X"))), List(Operation(Eq(), List(Number(6), Operation(Sub(), List(Operation(Mult(), List(Variable("X"), Number(2))), Number(2))))))) 
    ))
    val r1 = p.solve(Predicate("a", List(Number(7))))
    assertEquals(None, r1.solution) 
    val r2 = p.solve(Predicate("a", List(Number(4))))
    assertEquals(Substitutions.trivial, r2.solution)  
  }

}