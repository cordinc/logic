package com.cordinc.logic

import org.junit._
import junit.framework.TestCase
import org.junit.Assert._

class ClauseTest extends TestCase {

  @Test def testFact: Unit = {
    assertTrue(HornClause.fact(Atom("f")).isFact)
    assertTrue(HornClause(Atom("f"), List()).isFact)
    assertFalse(HornClause(Atom("f"), List(Atom("d"))).isFact)
  }
  
  @Test def testHeadRename: Unit =  {
    assertEquals(HornClause.fact(Atom("a")), HornClause.fact(Atom("a")).rename(Set()))
    assertEquals(HornClause.fact(Variable("X")), HornClause.fact(Variable("X")).rename(Set()))
    assertEquals(HornClause.fact(Variable("X")), HornClause.fact(Variable("X")).rename(Set(Variable("Y"))))
    assertEquals(HornClause.fact(Variable("X1")), HornClause.fact(Variable("X")).rename(Set(Variable("X"))))
    assertEquals(HornClause.fact(Variable("X1")), HornClause.fact(Variable("X")).rename(Set(Variable("X"), Variable("X2"))))
  }
  
  @Test def testClauseRename: Unit =  {
    assertEquals(HornClause(Atom("a"), List(Atom("b"), Atom("c"))), HornClause(Atom("a"), List(Atom("b"), Atom("c"))).rename(Set()))
    assertEquals(HornClause(Atom("a"), List(Atom("b"), Atom("c"))), HornClause(Atom("a"), List(Atom("b"), Atom("c"))).rename(Set(Variable("X"))))
    
    assertEquals(HornClause(Variable("X"), List(Atom("b"), Atom("c"))), HornClause(Variable("X"), List(Atom("b"), Atom("c"))).rename(Set()))
    assertEquals(HornClause(Variable("X1"), List(Atom("b"), Atom("c"))), HornClause(Variable("X"), List(Atom("b"), Atom("c"))).rename(Set(Variable("X"))))
  
    assertEquals(HornClause(Variable("X"), List(Atom("b"), Variable("X"))), HornClause(Variable("X"), List(Atom("b"), Variable("X"))).rename(Set()))
    assertEquals(HornClause(Variable("X1"), List(Atom("b"), Variable("X1"))), HornClause(Variable("X"), List(Atom("b"), Variable("X"))).rename(Set(Variable("X"))))

    assertEquals(HornClause(Variable("X3"), List(Atom("b"), Variable("X3"))), HornClause(Variable("X"), List(Atom("b"), Variable("X"))).rename(Set(Variable("X"), Variable("X1"), Variable("X2"))))
    assertEquals(HornClause(Variable("Y"), List(Atom("b"), Variable("X3"))), HornClause(Variable("Y"), List(Atom("b"), Variable("X"))).rename(Set(Variable("X"), Variable("X1"), Variable("X2"))))
    assertEquals(HornClause(Variable("Y1"), List(Atom("b"), Variable("X3"))), HornClause(Variable("Y1"), List(Atom("b"), Variable("X"))).rename(Set(Variable("X"), Variable("X1"), Variable("X2"), Variable("Y"))))
  }
  
  @Test def testPrintFact: Unit =  {
    assertEquals("X3.", HornClause.fact(Variable("X3")).toString)
  }
  
  @Test def testPrintBody: Unit =  {
    assertEquals("X3 :- b, X3.", HornClause(Variable("X3"), List(Atom("b"), Variable("X3"))).toString)
  }
}