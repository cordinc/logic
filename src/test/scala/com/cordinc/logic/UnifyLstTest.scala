package com.cordinc.logic

import org.junit.Assert.assertEquals
import org.junit.Test

import junit.framework.TestCase;

class UnifyLstTest extends TestCase {
  
  @Test def testUnifyEmptyWithEmpty: Unit = {
    assertEquals(Some(Substitutions(Map())), ListTerm.empty.unifyWith(ListTerm.empty))
  }
  
  @Test def testUnifyEmptyWithVar: Unit = {
    assertEquals(Some(Substitutions(Map(Variable("X") -> ListTerm.empty))), ListTerm.empty.unifyWith(Variable("X")))
    assertEquals(Some(Substitutions(Map(Variable("X") -> ListTerm.empty))), Variable("X").unifyWith(ListTerm.empty))
  }
  
  @Test def testUnifyEmptyWithAtom: Unit = {
    assertEquals(None, ListTerm.empty.unifyWith(Atom("a")))
    assertEquals(None, Atom("a").unifyWith(ListTerm.empty))
  }
  
  @Test def testUnifyEmptyWithSingletonList: Unit = {
    assertEquals(None, ListTerm.empty.unifyWith(Lst(List(Atom("a")))))
    assertEquals(None, Lst(List(Atom("a"))).unifyWith(ListTerm.empty))
  }
  
  @Test def testUnifyEmptyWithList: Unit = {
    assertEquals(None, ListTerm.empty.unifyWith(Lst(List(Atom("a"), Atom("b"), Atom("c")))))
    assertEquals(None, Lst(List(Atom("a"), Atom("b"), Atom("c"))).unifyWith(ListTerm.empty))
  }
  
  @Test def testUnifyEmptyWithHTList: Unit = {
    assertEquals(None, ListTerm.empty.unifyWith(HeadTailLst(Atom("a"), Atom("b"))))
    assertEquals(None, HeadTailLst(Atom("a"), Atom("b")).unifyWith(ListTerm.empty))
  }
  
  @Test def testUnifyEmptyWithHTVars: Unit = {
    assertEquals(None, ListTerm.empty.unifyWith(HeadTailLst(Variable("X"), Variable("Y"))))
    assertEquals(None, HeadTailLst(Variable("X"), Variable("Y")).unifyWith(ListTerm.empty))
  }
  
  @Test def testUnifyVarListWithList: Unit = {
    assertEquals(None, Lst(List(Variable("X"))).unifyWith(Lst(List(Atom("a"), Atom("b"), Atom("c")))))
    assertEquals(None, Lst(List(Atom("a"), Atom("b"), Atom("c"))).unifyWith(Lst(List(Variable("X")))))
  }
  
  @Test def testUnifyVarListWithSingletonList: Unit = {
    assertEquals(Some(Substitutions(Map(Variable("X") -> Atom("a")))), Lst(List(Variable("X"))).unifyWith(Lst(List(Atom("a")))))
    assertEquals(Some(Substitutions(Map(Variable("X") -> Atom("a")))), Lst(List(Atom("a"))).unifyWith(Lst(List(Variable("X")))))
  }
  
  @Test def testUnifyVarListWithEmptyList: Unit = {
    assertEquals(None, Lst(List(Variable("X"))).unifyWith(ListTerm.empty))
    assertEquals(None, ListTerm.empty.unifyWith(Lst(List(Variable("X")))))
  }
  
  @Test def testUnifyVarListWithHeadTailList: Unit = {
    assertEquals(None, Lst(List(Variable("X"))).unifyWith(HeadTailLst(Atom("a"), Atom("b"))))
    assertEquals(None, HeadTailLst(Atom("a"), Atom("b")).unifyWith(Lst(List(Variable("X")))))
  }
  
  @Test def testUnifyVarListWithVar: Unit = {
    assertEquals(None, Variable("X").unifyWith(Lst(List(Variable("X")))))
    assertEquals(None, Lst(List(Variable("X"))).unifyWith(Variable("X")))
  }
  
  @Test def testUnifySingletonLst: Unit = {
    assertEquals(Some(Substitutions(Map())), Lst(List(Atom("a"))).unifyWith(Lst(List(Atom("a")))))
  }
  
  @Test def testUnifySingletonLstWithLst: Unit = {
    assertEquals(None, Lst(List(Atom("a"))).unifyWith(Lst(List(Atom("a"), Atom("b"), Atom("c")))))
    assertEquals(None, Lst(List(Atom("a"), Atom("b"), Atom("c"))).unifyWith(Lst(List(Atom("a")))))
  }
  
  @Test def testUnifySingletonLstWithVar: Unit = {
    assertEquals(Some(Substitutions(Map(Variable("X") -> Lst(List(Atom("a"), Atom("b"), Atom("c")))))), Variable("X").unifyWith(Lst(List(Atom("a"), Atom("b"), Atom("c")))))
    assertEquals(Some(Substitutions(Map(Variable("X") -> Lst(List(Atom("a"), Atom("b"), Atom("c")))))), Lst(List(Atom("a"), Atom("b"), Atom("c"))).unifyWith(Variable("X")))
  }
  
  @Test def testUnifyHTLst: Unit = {
    assertEquals(Some(Substitutions(Map())), HeadTailLst(Atom("a"), Atom("b")).unifyWith(HeadTailLst(Atom("a"), Atom("b"))))
  }
  
  @Test def testUnifyHTLstDuped: Unit = {
    assertEquals(None, HeadTailLst(Atom("a"), Atom("a")).unifyWith(HeadTailLst(Atom("a"), Atom("b"))))
  }
  
  @Test def testUnifyHTLstWithVar: Unit = {
    assertEquals(Some(Substitutions(Map(Variable("X") -> HeadTailLst(Atom("a"), Atom("b"))))), HeadTailLst(Atom("a"), Atom("b")).unifyWith(Variable("X")))
    assertEquals(Some(Substitutions(Map(Variable("X") -> HeadTailLst(Atom("a"), Atom("b"))))), Variable("X").unifyWith(HeadTailLst(Atom("a"), Atom("b"))))
  }
  
  @Test def testUnifyHTLstWithSingletonList: Unit = {
    assertEquals(None, HeadTailLst(Atom("a"), Atom("b")).unifyWith(Lst(List(Atom("a")))))
    assertEquals(None, Lst(List(Atom("a"))).unifyWith(HeadTailLst(Atom("a"), Atom("b"))))
  }
  
  @Test def testUnifyHTLstWithList: Unit = {
    assertEquals(None, HeadTailLst(Atom("a"), Atom("b")).unifyWith(Lst(List(Atom("a"), Atom("b")))))
    assertEquals(None, Lst(List(Atom("a"), Atom("b"))).unifyWith(HeadTailLst(Atom("a"), Atom("b"))))
  }
  
  @Test def testUnifyHTLstWithLongerList: Unit = {
    assertEquals(None, HeadTailLst(Atom("a"), Atom("b")).unifyWith(Lst(List(Atom("a"), Atom("b"), Atom("c")))))
    assertEquals(None, Lst(List(Atom("a"), Atom("b"), Atom("c"))).unifyWith(HeadTailLst(Atom("a"), Atom("b"))))
  }
  
  @Test def testUnifyLstWithPred: Unit = {
    assertEquals(None, Lst(List(Atom("a"), Atom("b"))).unifyWith(Predicate("f", List(Atom("a"), Atom("b")))))
    assertEquals(None, Predicate("f", List(Atom("a"), Atom("b"))).unifyWith(Lst(List(Atom("a"), Atom("b")))))
  }
  
  @Test def testUnifyHTLstWithPred: Unit = {
    assertEquals(None, HeadTailLst(Atom("a"), Atom("b")).unifyWith(Predicate("f", List(Atom("a"), Atom("b")))))
    assertEquals(None, Predicate("f", List(Atom("a"), Atom("b"))).unifyWith(HeadTailLst(Atom("a"), Atom("b"))))
  }
  
  @Test def testUnifyHTLstWithHTVars: Unit = {
    assertEquals(Some(Substitutions(Map(Variable("X") -> Atom("a"), Variable("Y") -> Atom("b")))), HeadTailLst(Variable("X"), Variable("Y")).unifyWith(HeadTailLst(Atom("a"), Atom("b"))))
    assertEquals(Some(Substitutions(Map(Variable("X") -> Atom("a"), Variable("Y") -> Atom("b")))), HeadTailLst(Atom("a"), Atom("b")).unifyWith(HeadTailLst(Variable("X"), Variable("Y"))))
  }
  
  @Test def testUnifyHTVars: Unit = {
    assertEquals(Some(Substitutions(Map())), HeadTailLst(Variable("X"), Variable("Y")).unifyWith(HeadTailLst(Variable("X"), Variable("Y"))))
  }
  
  @Test def testUnifyHTVarsWithAtom: Unit = {
    assertEquals(None, HeadTailLst(Variable("X"), Variable("Y")).unifyWith(Atom("a")))
    assertEquals(None, Atom("a").unifyWith(HeadTailLst(Variable("X"), Variable("Y"))))
  }
  
  @Test def testUnifyHTVarsWithSingletonList: Unit = {
    assertEquals(Some(Substitutions(Map(Variable("X") -> Atom("a"), Variable("Y") -> ListTerm.empty))), HeadTailLst(Variable("X"), Variable("Y")).unifyWith(Lst(List(Atom("a")))))
    assertEquals(Some(Substitutions(Map(Variable("X") -> Atom("a"), Variable("Y") -> ListTerm.empty))), Lst(List(Atom("a"))).unifyWith(HeadTailLst(Variable("X"), Variable("Y"))))
  }
  
  @Test def testUnifyHTVarsWithList: Unit = {
    assertEquals(Some(Substitutions(Map(Variable("X") -> Atom("a"), Variable("Y") -> Lst(List(Atom("b")))))), HeadTailLst(Variable("X"), Variable("Y")).unifyWith(Lst(List(Atom("a"), Atom("b")))))
    assertEquals(Some(Substitutions(Map(Variable("X") -> Atom("a"), Variable("Y") -> Lst(List(Atom("b")))))), Lst(List(Atom("a"), Atom("b"))).unifyWith(HeadTailLst(Variable("X"), Variable("Y"))))
  }
  
  @Test def testUnifyHTVarsWithList2: Unit = {
    assertEquals(Some(Substitutions(Map(Variable("X") -> Atom("a"), Variable("Y") -> Lst(List(Atom("b"), Atom("c")))))), HeadTailLst(Variable("X"), Variable("Y")).unifyWith(Lst(List(Atom("a"), Atom("b"), Atom("c")))))
    assertEquals(Some(Substitutions(Map(Variable("X") -> Atom("a"), Variable("Y") -> Lst(List(Atom("b"), Atom("c")))))), Lst(List(Atom("a"), Atom("b"), Atom("c"))).unifyWith(HeadTailLst(Variable("X"), Variable("Y"))))
  }
  
  @Test def testUnifyHTVarsWithList3: Unit = {
    assertEquals(Some(Substitutions(Map(Variable("X") -> Atom("a"), Variable("Y") -> Lst(List(Atom("b"), Atom("c"), Atom("d")))))), HeadTailLst(Variable("X"), Variable("Y")).unifyWith(Lst(List(Atom("a"), Atom("b"), Atom("c"), Atom("d")))))
    assertEquals(Some(Substitutions(Map(Variable("X") -> Atom("a"), Variable("Y") -> Lst(List(Atom("b"), Atom("c"), Atom("d")))))), Lst(List(Atom("a"), Atom("b"), Atom("c"), Atom("d"))).unifyWith(HeadTailLst(Variable("X"), Variable("Y"))))
  }
  
  @Test def testUnifyHTVarsWithHTEmpty: Unit = {
    assertEquals(Some(Substitutions(Map(Variable("X") -> Atom("a"), Variable("Y") -> ListTerm.empty))), 
        HeadTailLst(Variable("X"), Variable("Y")).unifyWith(HeadTailLst(Atom("a"), ListTerm.empty)))
    assertEquals(Some(Substitutions(Map(Variable("X") -> Atom("a"), Variable("Y") -> ListTerm.empty))), 
        HeadTailLst(Atom("a"), ListTerm.empty).unifyWith(HeadTailLst(Variable("X"), Variable("Y"))))
  }
  
  @Test def testUnifyHTVarsWithHTList: Unit = {
    assertEquals(Some(Substitutions(Map(Variable("X") -> Atom("a"), Variable("Y") -> Lst(List(Atom("b"), Atom("c")))))), HeadTailLst(Variable("X"), Variable("Y")).unifyWith(HeadTailLst(Atom("a"), Lst(List(Atom("b"), Atom("c"))))))
    assertEquals(Some(Substitutions(Map(Variable("X") -> Atom("a"), Variable("Y") -> Lst(List(Atom("b"), Atom("c")))))), HeadTailLst(Atom("a"), Lst(List(Atom("b"), Atom("c")))).unifyWith(HeadTailLst(Variable("X"), Variable("Y"))))
  }
  
  @Test def testUnifyHTVarsWithHTVar: Unit = {
    assertEquals(Some(Substitutions(Map(Variable("X") -> Atom("a"), Variable("Y") -> Variable("T")))), HeadTailLst(Variable("X"), Variable("Y")).unifyWith(HeadTailLst(Atom("a"), Variable("T"))))
    assertEquals(Some(Substitutions(Map(Variable("X") -> Atom("a"), Variable("T") -> Variable("Y")))), HeadTailLst(Atom("a"), Variable("T")).unifyWith(HeadTailLst(Variable("X"), Variable("Y"))))
  }
  
  @Test def testUnifyHTVarsWithHTPred: Unit = {
    assertEquals(Some(Substitutions(Map(Variable("X") -> Atom("a"), Variable("Y") -> Predicate("f", List(Atom("a")))))), HeadTailLst(Variable("X"), Variable("Y")).unifyWith(HeadTailLst(Atom("a"), Predicate("f", List(Atom("a"))))))
    assertEquals(Some(Substitutions(Map(Variable("X") -> Atom("a"), Variable("Y") -> Predicate("f", List(Atom("a")))))), HeadTailLst(Atom("a"), Predicate("f", List(Atom("a")))).unifyWith(HeadTailLst(Variable("X"), Variable("Y"))))
  }
  
  @Test def testUnifyTVarsWithHT: Unit = {
    assertEquals(Some(Substitutions(Map(Variable("X") -> Atom("a"), Variable("Y") -> Variable("T")))), 
        HeadTailLst(Atom("a"), Variable("Y")).unifyWith(HeadTailLst(Variable("X"), Variable("T"))))
    assertEquals(Some(Substitutions(Map(Variable("X") -> Atom("a"), Variable("T") -> Variable("Y")))), 
        HeadTailLst(Variable("X"), Variable("T")).unifyWith(HeadTailLst(Atom("a"), Variable("Y"))))
  }
  
  @Test def testUnifyTVarsWithLstSingletonNullLst: Unit = {
    assertEquals(None, HeadTailLst(Atom("b"), Variable("X")).unifyWith(ListTerm.empty))
    assertEquals(None, ListTerm.empty.unifyWith(HeadTailLst(Atom("b"), Variable("X"))))
  }
  
  @Test def testUnifyTVarsWithLstSingleton: Unit = {
    assertEquals(Some(Substitutions(Map(Variable("X") -> ListTerm.empty))), HeadTailLst(Atom("a"), Variable("X")).unifyWith(Lst(List(Atom("a")))))
    assertEquals(Some(Substitutions(Map(Variable("X") -> ListTerm.empty))), Lst(List(Atom("a"))).unifyWith(HeadTailLst(Atom("a"), Variable("X"))))
  }
  
  @Test def testUnifyTVarsWithLstSingletonUnmatched: Unit = {
    assertEquals(None, HeadTailLst(Atom("b"), Variable("X")).unifyWith(Lst(List(Atom("a")))))
    assertEquals(None, Lst(List(Atom("a"))).unifyWith(HeadTailLst(Atom("b"), Variable("X"))))
  }
  
  @Test def testUnifyTVarsWithLst2: Unit = {
    assertEquals(Some(Substitutions(Map(Variable("X") -> Lst(List(Atom("b")))))), HeadTailLst(Atom("a"), Variable("X")).unifyWith(Lst(List(Atom("a"), Atom("b")))))
    assertEquals(Some(Substitutions(Map(Variable("X") -> Lst(List(Atom("b")))))), Lst(List(Atom("a"), Atom("b"))).unifyWith(HeadTailLst(Atom("a"), Variable("X"))))
  }
  
  @Test def testUnifyTVarsWithLst3: Unit = {
    assertEquals(Some(Substitutions(Map(Variable("X") -> Lst(List(Atom("b"), Atom("c")))))), HeadTailLst(Atom("a"), Variable("X")).unifyWith(Lst(List(Atom("a"), Atom("b"), Atom("c")))))
    assertEquals(Some(Substitutions(Map(Variable("X") -> Lst(List(Atom("b"), Atom("c")))))), Lst(List(Atom("a"), Atom("b"), Atom("c"))).unifyWith(HeadTailLst(Atom("a"), Variable("X"))))
  }
  
  @Test def testUnifyHVarsWithLst2: Unit = {
    assertEquals(None, HeadTailLst(Variable("X"), Atom("a")).unifyWith(Lst(List(Atom("c"), Atom("b"), Atom("a")))))
    assertEquals(None, Lst(List(Atom("c"), Atom("b"), Atom("a"))).unifyWith(HeadTailLst(Variable("X"), Atom("a"))))
  }
  
  @Test def testUnifyHVarsWithLst3: Unit = {
    assertEquals(None, HeadTailLst(Variable("X"), Atom("a")).unifyWith(Lst(List(Atom("b"), Atom("a")))))
    assertEquals(None, Lst(List(Atom("b"), Atom("a"))).unifyWith(HeadTailLst(Variable("X"), Atom("a"))))
  }
  
  @Test def testUnifyHVarsWithSingleton: Unit = {
    assertEquals(None, HeadTailLst(Variable("X"), Atom("a")).unifyWith(Lst(List(Atom("a")))))
    assertEquals(None, Lst(List(Atom("a"))).unifyWith(HeadTailLst(Variable("X"), Atom("a"))))
  }
  
  @Test def testUnifyHVarsWithHT: Unit = {
    assertEquals(Some(Substitutions(Map(Variable("X") -> Atom("b")))), HeadTailLst(Variable("X"), Atom("a")).unifyWith(HeadTailLst(Atom("b"), Atom("a"))))
    assertEquals(Some(Substitutions(Map(Variable("X") -> Atom("b")))), HeadTailLst(Atom("b"), Atom("a")).unifyWith(HeadTailLst(Variable("X"), Atom("a"))))
  }
  
  @Test def testUnify2VarsWithSingleton: Unit = {
    assertEquals(None, Lst(List(Variable("X"), Variable("Y"))).unifyWith(Lst(List(Atom("a")))))
    assertEquals(None, Lst(List(Atom("a"))).unifyWith(Lst(List(Variable("X"), Variable("Y")))))
  }
  
  @Test def testUnify2VarsWithLst2: Unit = {
    assertEquals(Some(Substitutions(Map(Variable("X") -> Atom("a"), Variable("Y") -> Atom("b")))), Lst(List(Variable("X"), Variable("Y"))).unifyWith(Lst(List(Atom("a"), Atom("b")))))
    assertEquals(Some(Substitutions(Map(Variable("X") -> Atom("a"), Variable("Y") -> Atom("b")))), Lst(List(Atom("a"), Atom("b"))).unifyWith(Lst(List(Variable("X"), Variable("Y")))))
  }

  @Test def testUnify2VarsWithLst3: Unit = {
    assertEquals(None, Lst(List(Variable("X"), Variable("Y"))).unifyWith(Lst(List(Atom("a"), Atom("b"), Atom("c")))))
    assertEquals(None, Lst(List(Atom("a"), Atom("b"), Atom("c"))).unifyWith(Lst(List(Variable("X"), Variable("Y")))))
  }
  
  @Test def testUnifyHHTVarsWithEmpty: Unit = {
    assertEquals(None, HeadTailLst(Variable("X"), HeadTailLst(Variable("Y"), Variable("Z"))).unifyWith(ListTerm.empty))
    assertEquals(None, ListTerm.empty.unifyWith(HeadTailLst(Variable("X"), HeadTailLst(Variable("Y"), Variable("Z")))))
  }
  
  @Test def testUnifyHHTVarsWithSingleton: Unit = {
    assertEquals(None, HeadTailLst(Variable("X"), HeadTailLst(Variable("Y"), Variable("Z"))).unifyWith(Lst(List(Atom("a")))))
    assertEquals(None, Lst(List(Atom("a"))).unifyWith(HeadTailLst(Variable("X"), HeadTailLst(Variable("Y"), Variable("Z")))))
  }
  
  @Test def testUnifyHHTVarsWithLst2: Unit = {
    assertEquals(Some(Substitutions(Map(Variable("X") -> Atom("a"), Variable("Y") -> Atom("b"), Variable("Z") -> ListTerm.empty))), HeadTailLst(Variable("X"), HeadTailLst(Variable("Y"), Variable("Z"))).unifyWith(Lst(List(Atom("a"), Atom("b")))))
    assertEquals(Some(Substitutions(Map(Variable("X") -> Atom("a"), Variable("Y") -> Atom("b"), Variable("Z") -> ListTerm.empty))), Lst(List(Atom("a"), Atom("b"))).unifyWith(HeadTailLst(Variable("X"), HeadTailLst(Variable("Y"), Variable("Z")))))
  }
  
  @Test def testUnifyHHTVarsWithLst3: Unit = {
    assertEquals(Some(Substitutions(Map(Variable("X") -> Atom("a"), Variable("Y") -> Atom("b"), Variable("Z") -> Lst(List(Atom("c")))))), 
        HeadTailLst(Variable("X"), HeadTailLst(Variable("Y"), Variable("Z"))).unifyWith(Lst(List(Atom("a"), Atom("b"), Atom("c")))))
    assertEquals(Some(Substitutions(Map(Variable("X") -> Atom("a"), Variable("Y") -> Atom("b"), Variable("Z") -> Lst(List(Atom("c")))))), Lst(List(Atom("a"), Atom("b"), Atom("c"))).unifyWith(HeadTailLst(Variable("X"), HeadTailLst(Variable("Y"), Variable("Z")))))
  }
  
  @Test def testUnifyHHTVarsWithLst4: Unit = {
    assertEquals(Some(Substitutions(Map(Variable("X") -> Atom("a"), Variable("Y") -> Atom("b"), Variable("Z") -> Lst(List(Atom("c"), Atom("d")))))), HeadTailLst(Variable("X"), HeadTailLst(Variable("Y"), Variable("Z"))).unifyWith(Lst(List(Atom("a"), Atom("b"), Atom("c"), Atom("d")))))
    assertEquals(Some(Substitutions(Map(Variable("X") -> Atom("a"), Variable("Y") -> Atom("b"), Variable("Z") -> Lst(List(Atom("c"), Atom("d")))))), Lst(List(Atom("a"), Atom("b"), Atom("c"), Atom("d"))).unifyWith(HeadTailLst(Variable("X"), HeadTailLst(Variable("Y"), Variable("Z")))))
  }
  
  @Test def testUnifyHHTVarsWithLstOfLst: Unit = {
    assertEquals(Some(Substitutions(Map(Variable("X") -> Atom("a"), Variable("Y") -> Lst(List(Atom("b"), Atom("c"))), Variable("Z") -> ListTerm.empty))), 
        HeadTailLst(Variable("X"), HeadTailLst(Variable("Y"), Variable("Z"))).unifyWith(Lst(List(Atom("a"), Lst(List(Atom("b"), Atom("c")))))))
    assertEquals(Some(Substitutions(Map(Variable("X") -> Atom("a"), Variable("Y") -> Lst(List(Atom("b"), Atom("c"))), Variable("Z") -> ListTerm.empty))), 
        Lst(List(Atom("a"), Lst(List(Atom("b"), Atom("c"))))).unifyWith(HeadTailLst(Variable("X"), HeadTailLst(Variable("Y"), Variable("Z")))))
  }
  
  @Test def testUnifyHHTVarsWithHTHT: Unit = {
    assertEquals(Some(Substitutions(Map(Variable("X") -> Atom("a"), Variable("Y") -> HeadTailLst(Atom("b"), Atom("c")), Variable("Z") -> ListTerm.empty))), 
        HeadTailLst(Variable("X"), HeadTailLst(Variable("Y"), Variable("Z"))).unifyWith(Lst(List(Atom("a"), HeadTailLst(Atom("b"), Atom("c"))))))
    assertEquals(Some(Substitutions(Map(Variable("X") -> Atom("a"), Variable("Y") -> HeadTailLst(Atom("b"), Atom("c")), Variable("Z") -> ListTerm.empty))), 
        Lst(List(Atom("a"), HeadTailLst(Atom("b"), Atom("c")))).unifyWith(HeadTailLst(Variable("X"), HeadTailLst(Variable("Y"), Variable("Z")))))
  }
  
  @Test def testUnifyLstHTVarsWithLst3: Unit = {
    assertEquals(None, Lst(List(Atom("a"), Atom("b"), Atom("c"))).unifyWith(Lst(List(Atom("a"), HeadTailLst(Variable("X"), Variable("Y")), Atom("c")))))
    assertEquals(None, Lst(List(Atom("a"), HeadTailLst(Variable("X"), Variable("Y")), Atom("c"))).unifyWith(Lst(List(Atom("a"), Atom("b"), Atom("c")))))
  }
  
  @Test def testUnifyLstHTVarsWithLst4: Unit = {
    assertEquals(None, Lst(List(Atom("a"), Atom("b"), Atom("c"), Atom("c"))).unifyWith(Lst(List(Atom("a"), HeadTailLst(Variable("X"), Variable("Y")), Atom("c")))))
    assertEquals(None, Lst(List(Atom("a"), HeadTailLst(Variable("X"), Variable("Y")), Atom("c"))).unifyWith(Lst(List(Atom("a"), Atom("b"), Atom("c"), Atom("c")))))
  }
  
  @Test def testUnifyLstHTVarsWithSubLst: Unit = {
    assertEquals(Some(Substitutions(Map(Variable("X") -> Atom("b"), Variable("Y") -> ListTerm.empty))), Lst(List(Atom("a"), Lst(List(Atom("b"))), Atom("c"))).unifyWith(Lst(List(Atom("a"), HeadTailLst(Variable("X"), Variable("Y")), Atom("c")))))
    assertEquals(Some(Substitutions(Map(Variable("X") -> Atom("b"), Variable("Y") -> ListTerm.empty))), Lst(List(Atom("a"), HeadTailLst(Variable("X"), Variable("Y")), Atom("c"))).unifyWith(Lst(List(Atom("a"), Lst(List(Atom("b"))), Atom("c")))))
  }
  
  @Test def testUnifyLstHTVarsWithSubLst4: Unit = {
    assertEquals(Some(Substitutions(Map(Variable("X") -> Atom("b"), Variable("Y") -> Lst(List(Atom("d"), Atom("e")))))), Lst(List(Atom("a"), Lst(List(Atom("b"), Atom("d"), Atom("e"))), Atom("c"))).unifyWith(Lst(List(Atom("a"), HeadTailLst(Variable("X"), Variable("Y")), Atom("c")))))
    assertEquals(Some(Substitutions(Map(Variable("X") -> Atom("b"), Variable("Y") -> Lst(List(Atom("d"), Atom("e")))))), Lst(List(Atom("a"), HeadTailLst(Variable("X"), Variable("Y")), Atom("c"))).unifyWith(Lst(List(Atom("a"), Lst(List(Atom("b"), Atom("d"), Atom("e"))), Atom("c")))))
  }
  
  @Test def testUnifyLstHTVarsWithSubLstNull: Unit = {
    assertEquals(None, Lst(List(Atom("a"), ListTerm.empty, Atom("c"))).unifyWith(Lst(List(Atom("a"), HeadTailLst(Variable("X"), Variable("Y")), Atom("c")))))
    assertEquals(None, Lst(List(Atom("a"), HeadTailLst(Variable("X"), Variable("Y")), Atom("c"))).unifyWith(Lst(List(Atom("a"), ListTerm.empty, Atom("c")))))
  }
  
  @Test def testUnifyLstHTSingleVarsWithSelf: Unit = {
    assertEquals(Some(Substitutions(Map())), HeadTailLst(Atom("a"), Lst(List(Atom("b"), Variable("X")))).unifyWith(HeadTailLst(Atom("a"), Lst(List(Atom("b"), Variable("X"))))))
  }
  
  @Test def testUnifyLstHTSingleVarsWithAtom: Unit = {
    assertEquals(Some(Substitutions(Map(Variable("X") -> Atom("c")))), HeadTailLst(Atom("a"), Lst(List(Atom("b"), Variable("X")))).unifyWith(HeadTailLst(Atom("a"), Lst(List(Atom("b"), Atom("c"))))))
    assertEquals(Some(Substitutions(Map(Variable("X") -> Atom("c")))), HeadTailLst(Atom("a"), Lst(List(Atom("b"), Atom("c")))).unifyWith(HeadTailLst(Atom("a"), Lst(List(Atom("b"), Variable("X"))))))
  }
 
}