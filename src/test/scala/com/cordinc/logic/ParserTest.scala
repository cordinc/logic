package com.cordinc.logic

import org.junit.Assert.assertEquals
import org.junit.Assert.fail
import org.junit.Test

import junit.framework.TestCase

class ParserTest extends TestCase {
  
  @Test def testTerm: Unit = {
    assertEquals(True(), Parser.parseTerm("true"))
    assertEquals(Atom("a"), Parser.parseTerm("a"))
    assertEquals(Atom("atomX"), Parser.parseTerm("atomX"))
    assertEquals(False(), Parser.parseTerm("false"))
    assertEquals(False(), Parser.parseTerm("fail"))
    assertEquals(Cut(None), Parser.parseTerm("!"))
    assertEquals(Number(99.25), Parser.parseTerm("99.25"))
    assertEquals(Number(99), Parser.parseTerm("99"))
    assertEquals(Str("string"), Parser.parseTerm("\"string\""))
    assertEquals(Not(Atom("a")), Parser.parseTerm("not a"))
    assertEquals(Variable("X"), Parser.parseTerm("X"))
    assertEquals(Variable("Variable9"), Parser.parseTerm("Variable9"))
    assertEquals(Variable("_X"), Parser.parseTerm("_X"))
    assertEquals(Predicate("a", List(Variable("X"))), Parser.parseTerm("a(X)"))
    assertEquals(Lst(List()), Parser.parseTerm("[]"))
    assertEquals(Predicate("other1", List(Variable("X"), Atom("a"), Lst(List(Atom("b"), Atom("c"))))), Parser.parseTerm("other1(X, a, [b, c])"))
    assertEquals(HeadTailLst(Variable("X"), Variable("Y")), Parser.parseTerm("[X|Y]"))
  }
  
  @Test def testTermFailure: Unit = {
    try {
      Parser.parseTerm("come on")
      fail
    } catch {
      case e: IllegalArgumentException => {}
      case _ : Throwable => fail
    }
  }
  
  @Test def testTermFailureQuery: Unit = {
    try {
      Parser.parseTerm("atomX.")
      fail
    } catch {
      case e: IllegalArgumentException => {}
      case _ : Throwable => fail
    }
  }
  
  @Test def testOperatorTerm: Unit = {
    assertEquals(Operation(Eq(), List(Variable("X"), Number(4))), Parser.parseTerm("=(X, 4)"))
    assertEquals(Operation(Eq(), List(Variable("X"), Str("abc"))), Parser.parseTerm("=(X,\"abc\")"))
    assertEquals(Operation(Add(), List(Variable("Y"), Number(-4))), Parser.parseTerm("+(Y,-4)"))
    assertEquals(Operation(Sub(), List(Variable("X"), Number(7))), Parser.parseTerm("-(X,7)"))
    assertEquals(Operation(Mult(), List(Variable("Z"), Number(4))), Parser.parseTerm("*(Z,4)"))
    assertEquals(Operation(Div(), List(Number(8), Variable("X"))), Parser.parseTerm("/(8,X)"))
    assertEquals(Operation(Gt(), List(Number(4), Variable("AA"))), Parser.parseTerm(">(4,AA)"))
    assertEquals(Operation(Lt(), List(Variable("X"), Number(4))), Parser.parseTerm("<(X,4)"))
    assertEquals(Operation(GtEq(), List(Number(8), Variable("B"))), Parser.parseTerm(">=(8,B)"))
    assertEquals(Operation(LtEq(), List(Number(4), Variable("X"))), Parser.parseTerm("<=(4,X)"))
    
    assertEquals(Operation(Eq(), List(Number(6), Operation(Add(), List(Operation(Div(), List(Variable("X"), Number(2))), Number(1))))), Parser.parseTerm("=(6,+(/(X,2),1))"))
  }
  
  @Test def testQueryTerm: Unit = {
    assertEquals(True(), Parser.parseQuery("true."))
    assertEquals(Atom("a"), Parser.parseQuery("a."))
    assertEquals(Atom("atomX"), Parser.parseQuery("atomX."))
    assertEquals(False(), Parser.parseQuery("false."))
    assertEquals(Predicate("other1", List(Variable("X"), Atom("a"), Lst(List(Atom("b"), Atom("c"))))), Parser.parseQuery("other1(X, a, [b, c])."))
  }
  
  @Test def testQueryFailure: Unit = {
    try {
      Parser.parseQuery("atomX")
      fail
    } catch {
      case e: IllegalArgumentException => {}
      case _ : Throwable => fail
    }
  }
  
  @Test def testProgramTrivial: Unit = {
    val a = Program(List(HornClause.fact(Atom("a"))))
    val b = Parser.parseProgram("a.") 
    assertEquals(a, b)
  }
  
  @Test def testProgramSimple: Unit = {
    val a = Program(List(
      HornClause(Predicate("parent", List(Variable("X"), Variable("Y"))), List(Predicate("father", List(Variable("X"), Variable("Y"))), Predicate("alive", List(Variable("X"))))),   
      HornClause(Predicate("parent", List(Variable("X"), Variable("Y"))), List(Predicate("mother", List(Variable("X"), Variable("Y"))), Predicate("alive", List(Variable("X")))))))
    val b = Parser.parseProgram("parent(X,Y):-father(X,Y), alive(X).parent(X,Y):-mother(X,Y), alive(X).") 
    assertEquals(a, b)
  }

}