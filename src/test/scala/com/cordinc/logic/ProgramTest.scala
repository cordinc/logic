package com.cordinc.logic

import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test

import junit.framework.TestCase;

class ProgramTest extends TestCase {
  
  implicit val tracer = new NullTracer()
  
  @Test def testAdd: Unit = {    
    assertEquals(Program(List(HornClause.fact(Atom("a")))),
        Program(List()) + HornClause.fact(Atom("a")))
    assertEquals(Program(List(HornClause.fact(Atom("a")), HornClause.fact(Atom("b")))),
        Program(List(HornClause.fact(Atom("a")))) + HornClause.fact(Atom("b")))
    assertEquals(Program(List(HornClause.fact(Atom("a")), HornClause.fact(Atom("b")), HornClause.fact(Atom("c")))),
        Program(List(HornClause.fact(Atom("a")))).add(HornClause.fact(Atom("b")), HornClause.fact(Atom("c"))))
  }
  
  @Test def testPrint: Unit = {
    val test = Program(List(
      HornClause(Predicate("a", List(Variable("X"))), List(Predicate("b", List(Variable("X"))), Cut(None), Predicate("c", List(Variable("X"))))),
      HornClause(Predicate("a", List(Variable("X"))), List(Predicate("d", List(Variable("X"))))),
      HornClause.fact(Predicate("b", List(Atom("1")))),
      HornClause.fact(Predicate("c", List(Atom("1")))),
      HornClause.fact(Predicate("d", List(Atom("2"))))
    ))
    
    assertEquals("a(X) :- b(X), !, c(X).\na(X) :- d(X).\nb(1).\nc(1).\nd(2).", test.toString)
  }
  
  @Test def testAddProgram: Unit = { 
    assertEquals(Program(List(HornClause.fact(Atom("a")))),
        Program(List()) + Program(List(HornClause.fact(Atom("a")))))
    assertEquals(Program(List(HornClause.fact(Atom("a")), HornClause.fact(Atom("b")))),
        Program(List(HornClause.fact(Atom("a")))) + Program(List(HornClause.fact(Atom("b")))))
    assertEquals(Program(List(HornClause.fact(Atom("a")), HornClause.fact(Atom("b")), HornClause.fact(Atom("c")))),
        Program(List(HornClause.fact(Atom("a"))))+ Program(List(HornClause.fact(Atom("b")), HornClause.fact(Atom("c")))))
  }
  
  @Test def testCut: Unit = { 
    val test = Program(List(
      HornClause(Predicate("a", List(Variable("X"))), List(Predicate("b", List(Variable("X"))), Cut(None), Predicate("c", List(Variable("X"))))),
      HornClause(Predicate("a", List(Variable("X"))), List(Predicate("d", List(Variable("X"))))),
      HornClause.fact(Predicate("b", List(Atom("1")))),
      HornClause.fact(Predicate("c", List(Atom("1")))),
      HornClause.fact(Predicate("d", List(Atom("2"))))
    ))
    
    val result = test.solve(Predicate("a", List(Variable("X"))))
    assertEquals(Some(Substitutions(Map(Variable("X") -> Atom("1")))), result.solution)
    assertTrue(result.maybeMore)
    assertFalse(result.nextResult.hasSolution)
  }
  
  @Test def testTrue: Unit = { 
    val test = Program(List(
      HornClause(Predicate("a", List(Variable("X"))), List(Predicate("b", List(Variable("X"))), True(), Predicate("c", List(Variable("X"))))),
      HornClause(Predicate("a", List(Variable("X"))), List(Predicate("d", List(Variable("X"))))),
      HornClause.fact(Predicate("b", List(Atom("1")))),
      HornClause.fact(Predicate("c", List(Atom("1")))),
      HornClause.fact(Predicate("d", List(Atom("2"))))
    ))
    
    val result = test.solve(Predicate("a", List(Variable("X"))))
    assertEquals(Some(Substitutions(Map(Variable("X") -> Atom("1")))), result.solution)
    assertTrue(result.maybeMore)
    assertEquals(Some(Substitutions(Map(Variable("X") -> Atom("2")))), result.nextResult.solution)
  }
  
  @Test def testCutBacktrack: Unit = { 
    val test = Program(List(
      HornClause(Predicate("a", List(Variable("X"), Variable("Y"))), List(Predicate("b", List(Variable("X"))), Cut(None), Predicate("c", List(Variable("Y"))))),
      HornClause.fact(Predicate("b", List(Atom("1")))),
      HornClause.fact(Predicate("b", List(Atom("2")))),
      HornClause.fact(Predicate("b", List(Atom("3")))),
      HornClause.fact(Predicate("c", List(Atom("1")))),
      HornClause.fact(Predicate("c", List(Atom("2")))),
      HornClause.fact(Predicate("c", List(Atom("3"))))
    ))
    
    val result = test.findAll(Predicate("a", List(Variable("X"), Variable("Y"))))
    assertEquals(4, result.size)
    assertEquals(Some(Substitutions(Map(Variable("X") -> Atom("1"), Variable("Y") -> Atom("1")))), result(0).solution)
    assertEquals(Some(Substitutions(Map(Variable("X") -> Atom("1"), Variable("Y") -> Atom("2")))), result(1).solution)
    assertEquals(Some(Substitutions(Map(Variable("X") -> Atom("1"), Variable("Y") -> Atom("3")))), result(2).solution)
    assertFalse(result(3).hasSolution)
  }
  
  @Test def testCutFail: Unit = { 
    val test = Program(List(
      HornClause(Predicate("a", List(Variable("X"))), List(Predicate("b", List(Variable("X"))), Cut(None), Predicate("c", List(Variable("X"))), False())),
      HornClause(Predicate("a", List(Variable("X"))), List(Predicate("d", List(Variable("X"))))),
      HornClause.fact(Predicate("b", List(Atom("1")))),
      HornClause.fact(Predicate("c", List(Atom("1")))),
      HornClause.fact(Predicate("d", List(Atom("2"))))
    ))
    
    assertEquals(None, test.solve(Predicate("a", List(Variable("X")))).solution)
  }
  
  @Test def testFail: Unit = { 
    val test = Program(List(
      HornClause(Predicate("a", List(Variable("X"))), List(Predicate("b", List(Variable("X"))), False())),
      HornClause(Predicate("a", List(Variable("X"))), List(Predicate("c", List(Variable("X"))))),
      HornClause.fact(Predicate("b", List(Atom("1")))),
      HornClause.fact(Predicate("c", List(Atom("2"))))
    ))

    assertEquals(None, test.solve(Predicate("a", List(Atom("1")))).solution)
    assertEquals(Substitutions.trivial, test.solve(Predicate("a", List(Atom("2")))).solution)
    assertEquals(Some(Substitutions(Map(Variable("X") -> Atom("2")))), test.solve(Predicate("a", List(Variable("X")))).solution)
  }
  
  @Test def testTrivialSolve: Unit = {
    assertEquals(Substitutions.trivial, Program(List(HornClause.fact(Atom("a")))).solve(Atom("a")).solution)
    assertTrue(Program(List(HornClause.fact(Atom("a")))).solve(Atom("a")).hasSolution) 
    assertEquals(None, Program(List(HornClause.fact(Atom("a")))).solve(Atom("b")).solution)
    assertEquals(None, Program.empty.solve(Atom("b")).solution)
    assertFalse(Program.empty.solve(Atom("b")).hasSolution) 
  }
  
}