package com.cordinc.logic

import org.junit.Assert.assertEquals
import org.junit.Test

import junit.framework.TestCase;

class SubstitutionsTest extends TestCase {
  
  @Test def testExtractVars: Unit = {
    assertEquals(Set.empty, Substitutions(Map()).extractVars)
    assertEquals(Set(Variable("X")), Substitutions(Map(Variable("X") -> Atom("b"))).extractVars)
    assertEquals(Set(Variable("X"), Variable("Y")), Substitutions(Map(Variable("X") -> Variable("Y"))).extractVars)
    assertEquals(Set(Variable("X"), Variable("Y"), Variable("Z")), Substitutions(Map(Variable("X") -> Variable("Y"), Variable("Z") -> HeadTailLst(Atom("a"), Term.f))).extractVars)
    assertEquals(Set(Variable("X"), Variable("Y"), Variable("Z")), Substitutions(Map(Variable("X") -> Variable("Y"), Variable("Z") -> Variable("X"))).extractVars)
    assertEquals(Set(Variable("X"), Variable("Y"), Variable("Z")), Substitutions(Map(Variable("X") -> Atom("a"), Variable("Y") -> Predicate("f", List(Variable("X"), Variable("Z"))))).extractVars)
  }
  
  @Test def testPush: Unit = {
    assertEquals(Substitutions(Map(Variable("Y") -> Lst(List(Atom("a"))))),
        Substitutions(Map()).push(Variable("Y"), Lst(List(Atom("a")))))
    assertEquals(Substitutions(Map(Variable("X") -> Atom("a"), Variable("Y") -> Lst(List(Atom("a"))))),
        Substitutions(Map(Variable("X") -> Atom("a"))).push(Variable("Y"), Lst(List(Atom("a")))))
    assertEquals(Substitutions(Map(Variable("X") -> Atom("a"), Variable("Y") -> Lst(List(Atom("a"))))),
        Substitutions(Map(Variable("X") -> Atom("a"))) :+ (Variable("Y"), Lst(List(Atom("a")))))
  }
  
  @Test def testAddAll: Unit = {
    assertEquals(Substitutions(Map(Variable("X") -> Atom("a"), Variable("Y") -> Lst(List(Atom("a"))))),
        Substitutions(Map(Variable("X") -> Atom("a"))).add(Substitutions(Map(Variable("Y") -> Lst(List(Atom("a")))))))
    assertEquals(Substitutions(Map(Variable("X") -> Atom("a"), Variable("Y") -> Lst(List(Atom("a"))))),
        Substitutions(Map(Variable("X") -> Atom("a"))) ++ Substitutions(Map(Variable("Y") -> Lst(List(Atom("a"))))))
  }
  
  @Test def testSubstitute: Unit = {
    assertEquals(Substitutions(Map(Variable("X") -> Atom("a"))),
        Substitutions(Map(Variable("X") -> Variable("Y"))).substitute(Variable("Y"), Atom("a")))
    assertEquals(Substitutions.empty, Substitutions.empty.substitute(Variable("Y"), Atom("a")))
  }
  
  @Test def testApplySingleReplace: Unit = {
    assertEquals(Lst(List(Atom("a"), Atom("a"))),
        Substitutions(Map(Variable("X") -> Atom("a"), Variable("Y") -> Lst(List(Atom("a"))))).applyTo(Lst(List(Atom("a"), Variable("X")))))
  }
  
  @Test def testApplyNoReplace: Unit = {
    assertEquals(Lst(List(Atom("a"), Variable("Z"))),
        Substitutions(Map(Variable("X") -> Atom("a"), Variable("Y") -> Lst(List(Atom("a"))))).applyTo(Lst(List(Atom("a"), Variable("Z")))))
  }
  
  @Test def testApplyRepeatedReplace: Unit = {
    assertEquals(Lst(List(Atom("a"), Atom("a"))),
        Substitutions(Map(Variable("X") -> Atom("a"), Variable("Y") -> Lst(List(Atom("a"))))).applyTo(Lst(List(Variable("X"), Variable("X")))))
  }
  
  @Test def testApplyDoubleReplace: Unit = {
    assertEquals(Lst(List(Atom("a"), Lst(List(Atom("a"))))),
        Substitutions(Map(Variable("X") -> Atom("a"), Variable("Y") -> Lst(List(Atom("a"))))).applyTo(Lst(List(Variable("X"), Variable("Y")))))
  }
  
  @Test def testApplyPredicateReplace: Unit = {   
    assertEquals(Predicate("f", List(Atom("a"), Predicate("g", List(Atom("b"))))),
        Substitutions(Map(Variable("X") -> Atom("a"), Variable("Y") -> Predicate("g", List(Atom("b"))))).applyTo(Predicate("f", List(Variable("X"), Variable("Y")))))
  }
  
  @Test def testUpdateWith: Unit = {
    assertEquals(Substitutions(Map(Variable("X") -> Atom("a"))),
        Substitutions(Map(Variable("X") -> Variable("Y"))).link(Substitutions(Map(Variable("Y") -> Atom("a")))))
    assertEquals(Substitutions(Map(Variable("X") -> Atom("a"))),
        Substitutions(Map(Variable("X") -> Variable("Y"))) ** Substitutions(Map(Variable("Y") -> Atom("a"))))
    assertEquals(Substitutions(Map(Variable("X") -> Variable("Y"))),
        Substitutions(Map(Variable("X") -> Variable("Y"))) ** Substitutions(Map(Variable("Z") -> Atom("a"))))
    assertEquals(Substitutions(Map(Variable("X") -> Atom("a"))),
        Substitutions(Map(Variable("X") -> Atom("a"))) ** Substitutions(Map(Variable("X") -> Atom("b"))))
  }
  
  @Test def testUpdateAdd: Unit = {
    assertEquals(Substitutions(Map(Variable("X") -> Atom("a"))),
        Substitutions(Map(Variable("X") -> Atom("a"))).combine(Substitutions(Map())))
    assertEquals(Substitutions(Map(Variable("X") -> Atom("a"), Variable("Y") -> Atom("b"))),
        Substitutions(Map(Variable("X") -> Atom("a"))).combine(Substitutions(Map(Variable("Y") -> Atom("b")))))
    assertEquals(Substitutions(Map(Variable("X") -> Variable("Y"), Variable("X") -> Atom("b"))),
        Substitutions(Map(Variable("X") -> Variable("Y"))).combine(Substitutions(Map(Variable("X") -> Atom("b")))))
    assertEquals(Substitutions(Map(Variable("X") -> Atom("b"), Variable("Y") -> Atom("b"))),
        Substitutions(Map(Variable("X") -> Variable("Y"))).combine(Substitutions(Map(Variable("Y") -> Atom("b")))))
    assertEquals(Substitutions(Map(Variable("X") -> Atom("b"), Variable("Y") -> Atom("b"), Variable("Z") -> Atom("c"))),
        Substitutions(Map(Variable("X") -> Variable("Y"), Variable("X") -> Variable("Y"))).combine(Substitutions(Map(Variable("Y") -> Atom("b"), Variable("Z") -> Atom("c")))))
  }
  
  @Test def testUpdateAddDeep: Unit = {
    assertEquals(Substitutions(Map(Variable("T") -> HeadTailLst(Atom("c"), Atom("a")), Variable("X") -> Lst(List(Atom("b"), Atom("x"), Lst(List(Atom("c"), Atom("a"))))))),
        Substitutions(Map(Variable("T") -> HeadTailLst(Atom("c"), Atom("a")))).combine(Substitutions(Map(Variable("X") -> HeadTailLst(Atom("b"), Lst(List(Atom("x"), Variable("T"))))))))

    assertEquals(Substitutions(Map(Variable("T") -> HeadTailLst(Atom("c"), Atom("a")), Variable("X") -> Lst(List(Atom("b"), Atom("c"), Atom("a"))))),
        Substitutions(Map(Variable("T") -> HeadTailLst(Atom("c"), Atom("a")))).combine(Substitutions(Map(Variable("X") -> HeadTailLst(Atom("b"), Variable("T"))))))
  }
}