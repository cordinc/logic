package com.cordinc.logic

import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test

import junit.framework.TestCase;

class ExampleTest extends TestCase {
  
  implicit val tracer = new NullTracer()

//  Hanoi requires operators to generate possible substitutions
//  @Test def testHanoi {
//    val hanoi = ("move(1,X,Y,_,[[X,Y]])."
//                   + "move(N,X,Y,Z,L):- >(N, 1), ==(M, -(N, 1)), move(M,X,Z,Y,L1), move(1,X,Y,_,L2), move(M,Z,Y,X,L3), append(L1,L2,L4), append(L4,L3,L). "
//                   + "append([],L,L)." 
//                   + "append([H|T],L2,[H|L3]):- append(T,L2,L3).").program
//    println(hanoi.solve("move(3,left,right,center,R).".query))
//  }
  
  @Test def testAppend: Unit =  {
    val append = ("append([],X,X)." 
                 +"append([H|T],X,[H|L]):- append(T,X,L).").program
    
    val result1 = append.solve("append([], a, X).".query)
    assertEquals(Some(Substitutions(Map(Variable("X") -> Atom("a")))), result1.solution)
    assertTrue(result1.maybeMore)
    assertFalse(result1.nextResult.hasSolution)      
    
    val result2 = append.solve("append([a], [b, c], X).".query)
    assertEquals(Some(Substitutions(Map(Variable("X") -> Lst(List(Atom("a"), Atom("b"), Atom("c")))))), result2.solution)
    assertTrue(result2.maybeMore)
    assertFalse(result2.nextResult.hasSolution)
    
    val result3 = append.solve("append([], a, []).".query)
    assertEquals(None, result3.solution)
    
    val result4 = append.solve("append(X, a, a).".query)
    assertEquals(Some(Substitutions(Map(Variable("X") -> Lst(List())))), result4.solution)
    assertTrue(result4.maybeMore)
    assertFalse(result4.nextResult.hasSolution)
    
    val result5 = append.solve("append(a, [b, c], X).".query)
    assertEquals(None, result5.solution)
    
    val result6 = append.solve("append([1, 2], [3, 4, 5], X).".query)
    assertEquals(Some(Substitutions(Map(Variable("X") -> Lst(List(Number(1), Number(2), Number(3), Number(4), Number(5)))))), result6.solution)
    assertTrue(result6.maybeMore)
    assertFalse(result6.nextResult.hasSolution)
  }
  
  @Test def testMember: Unit =  {
    val member = ("member(X, [X|_])."
                 +"member(X, [_|Tail]):- member(X, Tail).").program
                 
    val result1 = member.solve("member(X, [a, b]).".query)
    assertEquals(Some(Substitutions(Map(Variable("X") -> Atom("a")))), result1.solution)
    assertEquals(Some(Substitutions(Map(Variable("X") -> Atom("b")))), result1.nextResult.solution)
    assertFalse(result1.nextResult.nextResult.hasSolution)
    
    val result2 = member.solve("member(X, []).".query)
    assertFalse(result2.hasSolution)
    
    val result3 = member.solve("member(a, [b, c, d]).".query)
    assertFalse(result3.hasSolution)
    
    val result4 = member.solve("member(a, [b, a, d]).".query)
    assertTrue(result4.hasSolution)
    assertEquals(Substitutions.trivial, result4.solution)
  }
  
  @Test def testMax: Unit =  {
    val max = ("max(X,Y,Y)  :-  <=(X,  Y),!. "
              +"max(X,Y,X).").program
                 
    val result1 = max.solve("max(1, 2, X).".query)
    assertEquals(Some(Substitutions(Map(Variable("X") -> Number(2)))), result1.solution)
    
    val result2 = max.solve("max(3, 2, X).".query)
    assertEquals(Some(Substitutions(Map(Variable("X") -> Number(3)))), result2.solution)
    
    val result3 = max.solve("max(3, 2, 3).".query)
    assertTrue(result3.hasSolution)
    
    val result4 = max.solve("max(3, 2, 4).".query)
    assertFalse(result4.hasSolution)
  }
  
  /*
  human(socrates).	
	human(aristotle).
	human(plato).
	god(zeus).			
	god(apollo).
	mortal(X) :- human(X).	
	
	?-mortal(plato).		% is Plato mortal?
	yes
	?-mortal(apollo).		% is apollo mortal?
	no
	?-mortal(X).			% for which X is X mortal?
	X = socrates ->;
	X = aristotle ->;
	X = plato ->;
	no
	
	mortal_report :-
		write('Report of all known mortals'), nl, nl,
		mortal(X),
		write(X), nl,
		fail.
	mortal_report.
	
	?- mortal_report.
	Report of all known mortals
	socrates
	aristotle
	plato
	yes
	
	human_report :-
		write(heading),
		human(X),
		write(X),
		fail.
		
	*/
}