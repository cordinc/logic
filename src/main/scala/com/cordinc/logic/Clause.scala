package com.cordinc.logic

import scala.annotation.tailrec

sealed case class HornClause(head: Term, body: List[Term]) {
  @inline def isFact = body.length == 0 
  
  def rename[A](avoiding: Set[Variable]) = 
    if (avoiding.isEmpty) this else {
      val clauseVars = head.extractVars ++ body.flatMap(_.extractVars)
      val clashes = (clauseVars & avoiding)
      
      @tailrec def nextVar(base: Variable, idx: Int): Variable = {
        val v = Variable(base.name+idx)
        if (!clauseVars.contains(v) && !avoiding.contains(v)) v else nextVar(base, idx+1)
      }
      
      if (clashes.isEmpty) this else {
        val subs = Substitutions(clashes.map(s => s -> nextVar(s, 1)).toMap)
        HornClause(subs.applyTo(head), body.map(subs.applyTo))
      }
    }
  
  @inline def unifyWith(g: Term) = (body, g.unifyWith(head))
  
  override def toString = if (body.isEmpty) s"$head$clauseSeparator" else s"$head $impliesSeparator ${body.mkString(", ")}$clauseSeparator"
}

object HornClause {
  def fact(term: Term) = HornClause(term, List.empty) 
}