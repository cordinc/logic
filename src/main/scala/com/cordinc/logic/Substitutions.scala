package com.cordinc.logic

sealed case class Substitutions(subs: Map[Variable, Term]) {
  def substitute(v: Variable, term: Term) = Substitutions(subs.map(s => s._1 -> s._2.substitute(v, term))) 
  
  def push(v: Variable, term: Term) = Substitutions(subs + (v -> term))
  def :+(sub: (Variable, Term)) = push(sub._1, sub._2)
  
  def applyTo(t: Term) = subs.foldLeft (t) { (acc, i) => acc.substitute(i._1, i._2) }
  
  def add(ss: Substitutions) = Substitutions(subs ++ ss.subs) 
  def ++(ss: Substitutions) = add(ss)
  
  def combine(ss: Substitutions) = {
    def findMatches(t: Term, possibleSubs: Map[Variable, Term]) = t.extractVars.map(v => (v, possibleSubs.get(v))).collect {
      case (key, Some(value)) => key -> value
    }
    def substituteAll(t: Term, possibleSubs: Map[Variable, Term]) = findMatches(t, possibleSubs).foldLeft(t)((acc, s) => acc.substitute(s).compact)
    
    Substitutions(subs.map(s => s._1 -> substituteAll(s._2, ss.subs)) ++ ss.subs.map(s => s._1 -> substituteAll(s._2, subs)))
  }
  
  def link(ss: Substitutions) = Substitutions(subs.map(s => ss.subs.find(_._1 == s._2).map(s._1 -> s._2.substitute(_)).getOrElse(s)))
  def **(ss: Substitutions) = link(ss)    
  
  def extractVars = subs.foldLeft(Set.empty[Variable])((acc, s) => acc ++ s._2.extractVars + s._1)
}

object Substitutions {
  val empty = Substitutions(Map())
  val trivial = Some(empty)
}