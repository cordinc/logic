package com.cordinc.logic

trait Tracer {
  def noSolution(): Unit
  def solution(subs: Substitutions): Unit
  def backtrack(lvls: Seq[CheckpointLevel]): Unit
  def examine(g: Term): Unit
  def succeed(): Unit
  def fail(): Unit
  def cut(): Unit
  def notSuccess(g: Term): Unit
  def notFail(g: Term): Unit
  def noUnify(g: Term, c: HornClause): Unit
  def unify(g: Term, c: HornClause, ng: Seq[Term]): Unit
}

class NullTracer extends Tracer {
  @inline def noSolution() = {}
  @inline def solution(subs: Substitutions) = {}
  @inline def backtrack(lvls: Seq[CheckpointLevel]) = {}
  @inline def examine(g: Term) = {}
  @inline def succeed() = {}
  @inline def fail() = {}
  @inline def cut() = {}
  @inline def notSuccess(g: Term) = {}
  @inline def notFail(g: Term) = {}
  @inline def noUnify(g: Term, c: HornClause) = {}
  @inline def unify(g: Term, c: HornClause, ng: Seq[Term]) = {}
}

class PrintTracer extends Tracer {
  @inline def noSolution() = println("No solution")
  @inline def solution(subs: Substitutions) = println("Solution: "+subs)
  @inline def backtrack(lvls: Seq[CheckpointLevel]) = println("Backtrack: "+lvls.headOption.getOrElse("[]"))
  @inline def examine(g: Term) = println("Examine: "+g)
  @inline def succeed() = println("Succeed")
  @inline def fail() = println("Fail")
  @inline def cut() = println("Cut")
  @inline def notSuccess(g: Term) = println("Not succeeded: "+g)
  @inline def notFail(g: Term) = println("Not failed: "+g)
  @inline def noUnify(g: Term, c: HornClause) = println("Non-unifiable: "+g+" with: "+c)
  @inline def unify(g: Term, c: HornClause, ng: Seq[Term]) = println("Unifiable: "+g+" with: "+c+" giving goals: "+ng)
}

class CompoundTracer(tracers: Seq[Tracer]) extends Tracer {
  def noSolution() = tracers.foreach(_.noSolution)
  def solution(subs: Substitutions) = tracers.foreach(_.solution(subs))
  def backtrack(lvls: Seq[CheckpointLevel]) = tracers.foreach(_.backtrack(lvls))
  def examine(g: Term) = tracers.foreach(_.examine(g))
  @inline def succeed() = tracers.foreach(_.succeed)
  def fail() = tracers.foreach(_.fail)
  def cut() = tracers.foreach(_.cut)
  def notSuccess(g: Term) = tracers.foreach(_.notSuccess(g))
  def notFail(g: Term) = tracers.foreach(_.notFail(g))
  def noUnify(g: Term, c: HornClause) = tracers.foreach(_.noUnify(g, c))
  def unify(g: Term, c: HornClause, ng: Seq[Term]) = tracers.foreach(_.unify(g, c, ng))
}