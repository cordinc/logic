package com.cordinc.logic

import scala.annotation.tailrec

trait Result {
  def solution: Option[Substitutions]
  def hasSolution: Boolean
  def maybeMore: Boolean
  def nextResult(implicit trace: Tracer): Result
}

sealed case class Program(clauses: List[HornClause]) {
  
  def add(extra: HornClause*) = Program(clauses ++ extra)
  def +(extra: HornClause) = add(extra)
  def +(extra: Program) = add(extra.clauses:_*)
  
  sealed case class Checkpoints(orig: Substitutions, soln: Option[Substitutions], levels: Seq[CheckpointLevel]) extends Result {    
    @inline def maybeMore = !levels.isEmpty
    @inline def hasSolution = soln.isDefined
    @inline def solution = soln.map(orig ** _)
    @inline def nextResult(implicit trace: Tracer) = solve(Checkpoints(orig, None, levels))
    
    @inline def noSolution = Checkpoints(orig, None, List.empty)
    @inline def setSolution(subs: Substitutions, next: Seq[CheckpointLevel]) = Checkpoints(orig, Some(subs), next) 
    @inline def next(next: Seq[CheckpointLevel]) = Checkpoints(orig, soln, next) 
  }
  
  object Checkpoints {
    def initial(goal: Term, initialSubs: Substitutions) = Checkpoints(initialSubs, None, List((clauses, List(goal), initialSubs)))
  }
  
  @inline private def setCut(cutTo: Seq[CheckpointLevel])(t: Term): Term = t match {
    case Cut(_) => Cut(Some(cutTo))
    case x => x
  }
 
  @tailrec private def solve(cps: Checkpoints)(implicit trace: Tracer): Checkpoints = 
    if (cps.hasSolution) cps else cps.levels match {
      case Nil => { trace.noSolution; cps.noSolution } // no checkpoints so fail
      case (_, Nil, subs) :: lvls => { trace.solution(subs); cps.setSolution(subs, lvls) } // no goal to solve, so trivially succeed
      case (Nil, g :: _, _) :: lvls => { trace.backtrack(lvls); solve(cps.next(lvls)) } // checkpoint level empty, try next level     
      case (c :: cs, g :: gs, subs) :: lvls => { trace.examine(g); g } match {
        case True() => { trace.succeed; solve(cps.next((clauses, gs, subs) :: lvls)) }
        case False() => { trace.fail; solve(cps.next(lvls)) }
        case Cut(Some(cutTo)) => { trace.cut; solve(cps.next((c :: cs, gs, subs) +: cutTo)) }
        case Not(next) => if (solve(next).hasSolution) { trace.notFail(g); solve(cps.next(lvls)) } else { trace.notSuccess(g); solve(cps.next((cs, gs, subs) :: lvls)) }
        case _ => c.rename(subs.extractVars).unifyWith(g) match {
          case (_, None) => { trace.noUnify(g, c); solve(cps.next((cs, g :: gs, subs) :: lvls)) } // clause does not unify, try next clause in program
          case (ng, Some(ns)) => { // this goal is solved with dependencies, try to solve dependencies in a new level
            val nextGoals = (ng.map(setCut(lvls)) ++ gs).map(ns.applyTo)
            trace.unify(g, c, nextGoals)
            solve(cps.next((clauses, nextGoals, subs combine ns) :: (cs, g :: gs, subs) :: lvls))
          }
        }
      }
    }
  
  def solve(goal: Term)(implicit trace: Tracer): Result = solve(Checkpoints.initial(goal, Substitutions(goal.extractVars.map(s => s -> s).toMap)))
  
  @tailrec private def findAll(first: Result, acc: List[Result])(implicit trace: Tracer): List[Result] = 
    if (!first.maybeMore) acc :+ first else findAll(first.nextResult, acc :+ first)
    
  def findAll(goal: Term)(implicit trace: Tracer): List[Result] = findAll(solve(goal), List.empty)  

  override def toString = clauses.mkString("\n")
}

object Program {
  val empty = Program(List())
}