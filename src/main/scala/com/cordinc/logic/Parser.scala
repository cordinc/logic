package com.cordinc.logic

import scala.util.parsing.combinator._
import scala.util.parsing.input.CharSequenceReader
import scala.annotation.tailrec
import java.util.regex.Pattern

object Parser extends RegexParsers {
  
  val floatingPointNumber: Parser[String] = """-?(\d+(\.\d*)?|\d*\.\d+)([eE][+-]?\d+)?[fFdD]?""".r
  val stringText: Parser[String] = "\\w*".r
 
  def programElement: Parser[HornClause] = fact | clause
  def fact: Parser[HornClause] = (opTerm | predicateTerm | atomTerm) <~ clauseSeparator ^^ HornClause.fact
  def clause: Parser[HornClause] = (headTerm <~ impliesSeparator) ~ (repsep(bodyTerm, termSeparator) <~ clauseSeparator) ^^ { case h ~ b => HornClause(h, b) }
  
  def queryTerm: Parser[Term] = (trueTerm | falseTerm | predicateTerm | atomTerm) <~ clauseSeparator
  def headTerm: Parser[Term] = predicateTerm | atomTerm
  def bodyTerm: Parser[Term] = trueTerm | falseTerm | cutTerm | notTerm | opTerm | predicateTerm | atomTerm
  def term: Parser[Term] = trueTerm | falseTerm | cutTerm | notTerm | opTerm | numberTerm | stringTerm | variableTerm | predicateTerm | atomTerm | htListTerm | listTerm
  
  def trueTerm: Parser[Term] = True().toString ^^ { _ => True() }
  def falseTerm: Parser[Term] = (False().toString+"|fail").r ^^ { _ => False() }
  def cutTerm: Parser[Term] = Cut.label ^^ { _ => Cut(None) } 
  def notTerm: Parser[Term] = Not.label ~> term ^^ Not.apply 
  def numberTerm: Parser[Term] = floatingPointNumber ^^ { n => Number(n.toFloat) }  
  def stringTerm: Parser[Term] = stringSeparator ~> stringText <~ stringSeparator ^^ Str.apply   
  def variableTerm: Parser[Term] = Variable.nameRule ^^ Variable.apply
  def atomTerm: Parser[Term] = Atom.functorRule ^^ Atom.apply  
  def predicateTerm: Parser[Term] = Predicate.functorRule ~ (termListStart ~> repsep(term, termSeparator) <~ termListEnd) ^^ { case fn ~ args => Predicate(fn, args) }  
  def htListTerm: Parser[Term] = (lstStart ~> term) ~ ( htLstSeparator ~> term <~ lstEnd) ^^ { case h ~ t => HeadTailLst(h, t) } 
  def listTerm: Parser[Term] = lstStart ~> repsep(term, termSeparator) <~ lstEnd ^^ Lst.apply
  
  val ops = List(Eq(), Add(), Sub(), Mult(), Div(), Gt(), GtEq(), Lt(), LtEq())
  def opTerm: Parser[Term] = ops.map(parseOp).reduce(_|_)
  def parseOp(op: Operator) = op.functor ~> termListStart ~> repsep(term, termSeparator) <~ termListEnd ^^ { case params => Operation(op, params) } 
  
  def parseProgram(s: CharSequence): Program = {
    def extractClauses(s: CharSequence) = s.toString().split(Pattern.quote(clauseSeparator)).map(_.trim).filter(!_.isEmpty()).map(_+clauseSeparator)
    Program(extractClauses(s).map(parseClause).toList)
  }
  
  def parseClause(s: CharSequence): HornClause = phrase(programElement)(new CharSequenceReader(s)) match {
    case Success(t, _) => t
    case NoSuccess(msg, next) => throw new IllegalArgumentException("Could not parse '" + s + "' near '" + next.pos.longString + ": " + msg)
  }
  
  def parseQuery(s: CharSequence): Term = phrase(queryTerm)(new CharSequenceReader(s)) match {
    case Success(t, _) => t
    case NoSuccess(msg, next) => throw new IllegalArgumentException("Could not parse '" + s + "' near '" + next.pos.longString + ": " + msg)
  }
  
  def parseTerm(s: CharSequence): Term = phrase(term)(new CharSequenceReader(s)) match {
    case Success(t, _) => t
    case NoSuccess(msg, next) => throw new IllegalArgumentException("Could not parse '" + s + "' near '" + next.pos.longString + ": " + msg)
  }
}