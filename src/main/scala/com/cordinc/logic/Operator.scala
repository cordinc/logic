package com.cordinc.logic

sealed trait Operator {
  def functor: String
  def arity: Int
  def description: String
  def eval(args: Seq[Term]): Term
  def toString(args: Seq[Term]): String = s"$functor$termListStart${args.mkString(termSeparator)}$termListEnd"
  
  @inline protected def evalArgs(args: Seq[Term]) = args.take(arity).map(_ match {
    case o: Operation => o.op.eval(o.args)
    case x => x
  })
}

sealed case class Add() extends Operator {
  def functor = "+"
  def arity = 2
  def description = "add the second arg to the first"

  def eval(args: Seq[Term]) = evalArgs(args) match {
    case Number(x) :: Number(y) :: _ => Number(x+y) 
    case Number(x) :: Str(y) :: _ => Str(s"$x$y")  
    case Str(x) :: Number(y) :: _ => Str(x+y) 
    case args => Operation(this, args)
  }
}

sealed case class Sub() extends Operator {
  def functor = "-"
  def arity = 2
  def description = "subtract the second arg from the first"

  def eval(args: Seq[Term]) = evalArgs(args) match {
    case Number(x) :: Number(y) :: _ => Number(x-y) 
    case args => Operation(this, args)
  }
}

sealed case class Mult() extends Operator {
  def functor = "*"
  def arity = 2
  def description = "Multiple the second arg by the first"

  def eval(args: Seq[Term]) = evalArgs(args) match {
    case Number(x) :: Number(y) :: _ => Number(x*y) 
    case args => Operation(this, args)
  }
}

sealed case class Div() extends Operator {
  def functor = "/"
  def arity = 2
  def description = "Divide the first arg by the second"

  def eval(args: Seq[Term]) = evalArgs(args) match {
    case Number(x) :: Number(y) :: _ => Number(x/y) 
    case args => Operation(this, args)
  }
}

sealed case class Gt() extends Operator {
  def functor = ">"
  def arity = 2
  def description = "true iff first arg is greater than second"

  def eval(args: Seq[Term]) = evalArgs(args) match {
    case Number(x) :: Number(y) :: _ if x > y => Term.t
    case Number(x) :: Number(y) :: _ => Term.f
    case args => Operation(this, args)
  }
}

sealed case class Lt() extends Operator {
  def functor = "<"
  def arity = 2
  def description = "true iff first arg is less than second"

  def eval(args: Seq[Term]) = evalArgs(args) match {
    case Number(x) :: Number(y) :: _ if x < y => Term.t
    case Number(x) :: Number(y) :: _ => Term.f
    case args => Operation(this, args)
  }
}

sealed case class Eq() extends Operator {
  def functor = "="
  def arity = 2
  def description = "true iff first arg is equal to second"

  def eval(args: Seq[Term]) = evalArgs(args) match {
    // true if unifiable with trivial subs
    // true if both ground and the same, false is both ground and not the same
    // Op otherwise
    case (a:Term) :: (b:Term) :: _ if a.equals(b) => Term.t
    case args => Operation(this, args)
  }
}

sealed case class GtEq() extends Operator {
  def functor = ">="
  def arity = 2
  def description = "true iff first arg is equal greater than second"

  def eval(args: Seq[Term]) = evalArgs(args) match {
    case Number(x) :: Number(y) :: _ if x >= y => Term.t
    case Number(x) :: Number(y) :: _ => Term.f
    case args => Operation(this, args)
  }
}

sealed case class LtEq() extends Operator {
  def functor = "<="
  def arity = 2
  def description = "true iff first arg is equal or less than second"

  def eval(args: Seq[Term]) = evalArgs(args) match {
    case Number(x) :: Number(y) :: _ if x <= y => Term.t
    case Number(x) :: Number(y) :: _ => Term.f
    case args => Operation(this, args)
  }
}