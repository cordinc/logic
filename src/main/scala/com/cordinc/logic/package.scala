package com.cordinc

package object logic {
  
  type CheckpointLevel = (Seq[HornClause], Seq[Term], Substitutions)
  
  val clauseSeparator = "."
  val impliesSeparator = ":-"
  val termSeparator = ","
  val termListStart = "("
  val termListEnd = ")"
  val lstStart = "["
  val lstEnd = "]"
  val htLstSeparator = "|"
  val stringSeparator = "\""
  
  implicit class StringParsers(val s: String) {
    def term = Parser.parseTerm(s)
    def query = Parser.parseQuery(s)
    def program = Parser.parseProgram(s)
  }
}