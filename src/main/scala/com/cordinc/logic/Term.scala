package com.cordinc.logic

import scala.annotation.tailrec

sealed trait Term {
  def contains(x: Variable) = false
  def substitute(v: Variable, term: Term) = this
  def substitute(s: (Variable, Term)): Term = substitute(s._1, s._2)
  def extractVars = Set.empty[Variable]
  def compact = this
  
  def unifyWith(term: Term): Option[Substitutions] = {
     
    def unifyTerms(terms: (Term, Term), rem: List[(Term, Term)], subs: Option[Substitutions]): Option[(List[(Term, Term)], Option[Substitutions])] = {     
      def subTerms(v: Variable, term: Term) = rem.map(t => (t._1.substitute(v, term), t._2.substitute(v, term)))
      def appendSubs(v: Variable, term: Term) = subs.orElse(Substitutions.trivial).map(_.substitute(v, term)).map(_.push(v, term))
      def variableSubstitution(v: Variable, term: Term) = Some((subTerms(v, term), appendSubs(v, term)))
    
      terms match {
        case (x, y) if x == y => Some((rem, subs.orElse(Substitutions.trivial)))
        case (x: Variable, term) if !term.contains(x) => variableSubstitution(x, term)
        case (term, y: Variable) if !term.contains(y) => variableSubstitution(y, term)   
        case (x: ListTerm, y: ListTerm) if x.isEmpty && y.isEmpty => Some((rem, subs.orElse(Substitutions.trivial))) 
        case (x: ListTerm, y: ListTerm) if x.isEmpty || y.isEmpty => None
        case (x: ListTerm, y: ListTerm) => Some(((x.head, y.head) :: (x.tail, y.tail) :: rem, subs))
        case (x: CompoundTerm, y: CompoundTerm) if x unifiableWith y => Some((x.args.zip(y.args) ++: rem, subs))
        case (Not(x), Not(y)) => unifyTerms((x, y), rem, subs)
        case _ => None
      }
    }
    
    @tailrec def unify(terms: List[(Term, Term)], subs: Option[Substitutions]): Option[Substitutions] = 
      if (terms.isEmpty) subs else unifyTerms(terms.head, terms.tail, subs) match {
        case None => None
        case Some((nextTerms, nextSubs)) => unify(nextTerms, nextSubs)
      }
    
    unify(List((this, term)), None)
  }
}

object Term {
  val t = True()
  val f = False()
}

sealed case class True() extends Term {
  override def toString = "true"
}

sealed case class False() extends Term { 
  override def toString = "false"
}

sealed case class Cut(cutTo: Option[Seq[CheckpointLevel]]) extends Term {
  override def toString = Cut.label
}
object Cut {
  val label = "!"
}

sealed case class Not(negTerm: Term) extends Term {
  override def contains(x: Variable) = negTerm.contains(x)
  override def substitute(v: Variable, term: Term) = Not(negTerm.substitute(v, term))
  override def extractVars = negTerm.extractVars
  override def toString = Not.label+" "+negTerm
  override def compact = negTerm match {
    case Not(t) => t.compact
    case t => Not(t.compact)
  }
}
object Not {
  val label = "not"
}

sealed case class Variable(name: String) extends Term {
  require(!name.isEmpty)
  require(Variable.nameRule.pattern.matcher(name).matches)
  
  override def contains(x: Variable) = name == x.name
  override def substitute(v: Variable, term: Term) = if (name == v.name) term else this
  override def extractVars = Set(this)
  override def toString = name
}
object Variable {
  val nameRule = "[_A-Z]+\\w*".r // // variable functors can consist of any alphanumeric characters, but must start with a upper case letter or underscore
}

sealed case class Number(value: Double) extends Term {
  override def toString = value.toString
}

sealed case class Str(value: String) extends Term {
  override def toString = s"$stringSeparator$value$stringSeparator"
}

sealed case class Atom(functor: String) extends Term {
  require(!functor.isEmpty)
  require(Atom.functorRule.pattern.matcher(functor).matches)
  
  def arity = 0
  def args = List.empty
  override def toString = functor
}
object Atom {
  val functorRule = "[a-z0-9]+\\w*".r // atom functors can consist of any alphanumeric characters, but must start with a lower case letter or number
}

sealed trait CompoundTerm extends Term {
  def arity: Int
  def functor: String
  def args: Seq[Term] 
  override def extractVars = args.foldLeft(Set.empty[Variable])((acc, a) => acc ++ a.extractVars)
  def unifiableWith(other: CompoundTerm) = arity > 0 && arity == other.arity && functor == other.functor
}

sealed case class Predicate(functor: String, args: Seq[Term]) extends CompoundTerm {
  require(!args.isEmpty) // otherwise use Atom()
  require(!functor.isEmpty)
  require(Predicate.functorRule.pattern.matcher(functor).matches)
  
  def arity: Int = args.length
  override def contains(x: Variable) = args.exists(_.contains(x))
  override def substitute(v: Variable, term: Term) = Predicate(functor, args.map(_.substitute(v, term)))
  override def toString = s"$functor$termListStart${args.mkString(termSeparator)}$termListEnd"
  override def compact = Predicate(functor, args.map(_.compact))
}
object Predicate {
  val functorRule = "[a-z0-9]+\\w*".r // predicate functors can consist of any alphanumeric characters
}

sealed case class Operation(op: Operator, args: Seq[Term]) extends CompoundTerm {
  require(!args.isEmpty) // otherwise use Atom()
  require(args.size == op.arity)
  
  override def functor = op.functor
  def arity: Int = args.length
  override def contains(x: Variable) = args.exists(_.contains(x))
  override def substitute(v: Variable, term: Term) = op.eval(args.map(_.substitute(v, term)))
  override def toString = op.toString(args)
  override def compact = op.eval(args.map(_.compact))
}

sealed trait ListTerm extends CompoundTerm {
  def arity: Int = 2
  def functor: String = "."
  def isEmpty: Boolean
  def head: Term
  def tail: Term
  def args: Seq[Term]
}

object ListTerm {
  val empty = Lst(List())
}

sealed case class HeadTailLst(head: Term, tail: Term) extends ListTerm {
  override def isEmpty = false
  override def contains(x: Variable) = head.contains(x) || tail.contains(x)
  override def substitute(v: Variable, term: Term) = if (term == this) v else HeadTailLst(head.substitute(v, term), tail.substitute(v, term))
  override def args = List(head, tail)
  override def toString = s"$lstStart$head$htLstSeparator$tail$lstEnd"
  
  override def compact = (head, tail) match {
    case (h: Variable, t) => this
    case (_, t: Variable) => this
    case (h, Lst(t)) => Lst(h +: t.map(_.compact))
    case (h, t: HeadTailLst) => t.compact match {
      case Lst(l) => Lst(h +: l)
      case l: HeadTailLst => HeadTailLst(h, l)
      case t => Lst(List(h, t))
    }
    case (h, t) => Lst(List(h, t.compact))
  }
}

sealed case class Lst(values: Seq[Term]) extends ListTerm {  
  override def isEmpty = values.isEmpty
  
  @throws[NoSuchElementException]("if this is empty") 
  def head = values.head
  @throws[UnsupportedOperationException]("if this is empty")
  def tail = Lst(values.tail)
  
  override def contains(x: Variable) = values.exists(_.contains(x))
  override def substitute(v: Variable, term: Term) = if (term == this) v else Lst(values.map(_.substitute(v, term)))
  override def args = values
  override def toString = s"$lstStart${values.mkString(termSeparator)}$lstEnd"
  override def compact = Lst(values.map(_.compact))
}