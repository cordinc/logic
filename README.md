Logic
=====

A simple Scala implementation of [logical resolution](https://en.wikipedia.org/wiki/Resolution_(logic)) and is based on a small subset of [Prolog](https://en.wikipedia.org/wiki/Prolog). 

This is a project to teach myself Scala and remind myself about logic. Be careful using this in any production systems.


Todo
----

* add coverage report 
* allow write to stdout from within clauses
* check implementation of HeadTail lists
* be able to load libraries of programs
* allow disjunction in program clause body (use parser)
* tail recursion? see "Art of Prolog" Chap11
* extra logicals (io, types, filters, assert/retract...)
* anonymous vars & singletons with checks (in parser)
* operator unification as generator of potential substitutions, eg X>4 will return numbers above 5, then can do hanoi program as a test
* infix operators (how to parse?)
* more operators and easier to add 
* stop restricted words as predicates or atoms (eg fail, true, etc)
* '.' is both clause end marker and list functor - is this a problem?
* create a tracer that matches prolog logging
* create a tracer that counts actions

License
-------

See [LICENSE](LICENSE).