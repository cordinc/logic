name := "logic"

version := "0.2"

organization := "com.cordinc"

organizationName := "cordinc"

organizationHomepage := Some(url("https://www.cordinc.com"))

homepage := Some(url("https://www.cordinc.com"))

startYear := Some(2018)

description := "Simple logic system"

licenses += "MIT" -> url("https://opensource.org/licenses/MIT")

scalaVersion := "2.13.1"

scalacOptions := Seq("-feature", "-unchecked", "-deprecation", "-encoding", "utf8")

libraryDependencies += "com.novocode" % "junit-interface" % "0.11" % "test->default"

libraryDependencies += "org.scala-lang.modules" %% "scala-parser-combinators" % "1.1.2"
